<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/jslider.js"> </script>
  <script type = "text/javascript" src="javascript/slide.js"> </script>
    <title>PetMate Home</title>
  <%@page import="Application.*"%>
   <%@page import="bean.*"%>
    <%@page import="java.util.*"%>
   
   <% 	String ipAddress = request.getHeader("X-FORWARDED-FOR");  
	   	if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
	   	}
	   	String user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
	   	String  monthsString = request.getParameter("m");
	   	
	%>
</head>
<body >


<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="#" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="#" id="link-start" class="active"><li>Home</li></a>
                    <a href="adopt/search.jsp"  id="link-process" ><li>Adopt</li></a>
                    <a href="register/RegisterPet.jsp" id="link-portfolio" ><li>Rescue</li></a>
                    <a href="stats/" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>


<div id="container">  
  <div id="header">

    <div id="banner">
        <div id="slideshow">
            <img src="images/Banner1.jpg" alt="Slideshow Image 2" class="active" />
            <img src="images/Banner2.jpg" alt="Slideshow Image 2" />


        </div>

    </div>
</div>
</div>
  <div id="mainContent" class = "containerWidth">

<!--Titulo -->
    <h1>These pets are looking for home</h1>
  <div id="petResultframe">
   <%
   	int months = 0; 
    String pName  = request.getParameter("petname");
    String pType  = request.getParameter("petType");
    String pSubType  = request.getParameter("petSubTypex");
    String pCond = request.getParameter("condition");
    String pColor  = request.getParameter("color");
    String pSize  = request.getParameter("size");
    ArrayList<AppPet> matchingPets = new ArrayList<AppPet>();
    if (monthsString == null){
    	months = 3;
    }
    else{
    	months = Integer.parseInt(monthsString);
    }
    matchingPets = DbManager.homePagePets(months);
    if (matchingPets.size() > 0 ){
    for (int i = 0 ; i < matchingPets.size(); i++){
    	AppPet pet = matchingPets.get(i);
    	out.print("<div class='petResult'>");
    	ArrayList<String> imageList =  DbManager.loadPetImageSet(pet.getId());
    	if (imageList.size() > 0)out.print("<img src='images/"+imageList.get(0)+"'>");
    	else out.print("<img src='images/profilepic.jpg'>");
    	out.print("<table><tr><td>");
    	out.print("<h3>Name: <a href='adopt/viewPet.jsp?id="+pet.getId()+"'>");
    	out.print(pet.getName());out.print("</h3></a></td>");
    	out.print("<td><h3>Owner: <a href='user/viewuser.jsp?id="+pet.getGiver()+"'>");
    	out.print(pet.getGiver());out.print("</h3></a></td></tr>");
    	out.print("<tr><td><h3>Type: ");
    	out.print(pet.getType());out.print("</h3></td>");
    	out.print("<td><h3>Breed: ");
    	out.print(pet.getBreed());out.print("</h3></td></tr>");
    	out.print("<tr><td><h3>Condition: ");
    	out.print(pet.getCondition());out.print("</h3></td>");
    	out.print("<td><h3>Color: ");
    	out.print(pet.getColorA());out.print("</h3></td></tr>");
    	out.print("<tr><td><h3>Size: ");
    	out.print(pet.getSize());out.print("</h3></td><td></td></tr></table>");
    	out.print("</div>");
    }}
    %>
  
  </div>
</div>
<div class = "blueBg">
  <div class = "containerWidth">
    <h2>About the project</h2>
<h4><span class="superH">"</span>PetMate is a web application developed by students of the Tecnologico de Costa Rica. <br> 
</h4>
  </div>
</div>
</body>
</html>