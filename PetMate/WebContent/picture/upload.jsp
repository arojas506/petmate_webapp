<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/jslider.js"> </script>
  <script type = "text/javascript" src="javascript/slide.js"> </script>
    <title>Upload Picture</title>
  <%@page import="Application.AppSession"%>
   <%@page import="bean.DbManager"%>
   
   <% 	String ipAddress = request.getHeader("X-FORWARDED-FOR");  
	   	if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
	   	}
	   	String user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
	%>
</head>
<body >


<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="../" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../" id="link-start" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp"  id="link-process" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="link-portfolio" ><li>Rescue</li></a>
                    <a href="../stats/" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>



  <div id="mainContent" class = "containerWidth">
<%
String type  = request.getParameter("type");
String val  = request.getParameter("val");
%>
<h1>Select your new profile picture</h1>

<form method="post" action='${pageContext.request.contextPath}/Uploader' encType="multipart/form-data" id="upload">
<input type="hidden" name="pagename" value="uploadPicture"/>
<input type="file" name="file" value="select images..."/><br>
<input type="submit" class="blueButtn" value="start upload"/>
</form>
</div>
</body>
</html>