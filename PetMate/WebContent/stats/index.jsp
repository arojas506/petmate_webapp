<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/jslider.js"> </script>
  <script type = "text/javascript" src="javascript/slide.js"> </script>
    <title>PetMate stats</title>
  <%@page import="Application.AppSession"%>
   <%@page import="bean.DbManager"%>
   <%@page import="java.util.*"%>
   
   <% 	String ipAddress = request.getHeader("X-FORWARDED-FOR");  
	   	if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
	   	}
	   	String user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
	%>
</head>
<body >


<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="../" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../" id="link-start" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp"  id="link-process" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="link-portfolio" ><li>Rescue</li></a>
                    <a href="../stats/" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>


<div id="container">  

  <div id="mainContent" class = "containerWidth">

<!--Titulo -->
    <h1>Petmate's stats!</h1>
    <h2>General</h2>
    <table>
    <tr>
    <td><img src="images/1.png"/></td>
    <td><img src="images/2.png"/></td>
    <td><img src="images/3.png"/></td>
    </tr>
    <tr>
    <td>All pets</td>
    <td>Adopted</td>
    <td>Looking for Home</td>
    </tr>
    <td><% out.print(DbManager.stat1A());    %></td>
    <td><% out.print(DbManager.stat1B());    %> </td>
    <td><% out.print(DbManager.stat1C());    %></td>
    </table>
    <h2>Returned pets</h2>
    <table>
    <tr>
    <td class='tableheader'>Returned Pets</td>
    <td class='tableheader'># of pets returned</td>
    </tr>
    <td>
    <table class='subtable'>
    	<tr><td class='tableheader2'>Pet Name</td><td class='tableheader2'>Count</td></tr><tr>
		<%ArrayList<String> stats = new ArrayList<String>();
		stats=DbManager.stat2A();

			for (int i =0; i < stats.size(); i=i+2){
				out.print("<td>"+stats.get(i)+"</td><td>");
				if (stats.get(i+1)==null){
					out.print("0");
				}
				else{
					out.print(stats.get(i+1));
				}
						
				out.print("</td></tr>");
			}
				
				
		%></tr>
    </table>
    </td>
    <td><%
    
    if (DbManager.stat2B()==null){
		out.print("0");
	}
	else{
		out.print(DbManager.stat2B());
	}
    
    %></td>
    </table>
    <h2>Top Return Reasons</h2>
    <table>
    <tr>
    <td class='tableheader'>Reason</td>
    <td class='tableheader'>Count</td>
    </tr>
    <%
	stats=DbManager.stat3();
		for (int i =0; i < stats.size(); i=i+2){
		out.print("<td>"+stats.get(i)+"</td><td>");
		if (stats.get(i+1)==null){
			out.print("0");
		}
		else{
			out.print(stats.get(i+1));
		}
				
		out.print("</td></tr>");
		}		
		%></tr>
    </table>
    <h2>Top Users</h2>
    <table>
    <tr>
    <td class='tableheader'>User ID</td>
    <td class='tableheader'>Average Score</td>
    </tr>
    <%
	stats=DbManager.stat4();
		for (int i =0; i < stats.size(); i=i+2){
			out.print("<td>"+stats.get(i)+"</td><td>"+stats.get(i+1)+"</td></tr>");
		}		
		%></tr>
    </table>
     <h2>On Blacklist</h2>
    <table>
    <tr>
    <td class='tableheader'>User ID</td>
    </tr>
    <%
	stats=DbManager.stat5();
		for (int i =0; i < stats.size(); i++){
			out.print("<td>"+stats.get(i)+"</td></tr>");
		}		
		%></tr>
    </table>
</div>

</div>
</body>
</html>