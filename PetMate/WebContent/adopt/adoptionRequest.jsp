<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/hide.js"> </script>
��

 
  <%@page import="Application.*"%>
  <%@page import="bean.DbManager"%>
  <%@page import="java.util.*"%>
   
   <% 	
   String petId = request.getParameter("petId");
   System.out.println(petId);
   AppPet thisPet  = new AppPet();
   String user = new String();
   String name  = new String();
   if (!DbManager.isPet(petId)){
	   out.print("<meta http-equiv='refresh' content='0; url=../error/error.jsp?error=Pet not found' /></head></html>");
	   
   }else{
	   thisPet = DbManager.getPetProfile(Integer.parseInt(petId));
	   	String ipAddress = request.getHeader("X-FORWARDED-FOR");  
		if (ipAddress == null) {  
			ipAddress = request.getRemoteAddr();  
		}
		user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
		if (user.equals("")){
				 out.print("<meta http-equiv='refresh' content='0; url=../register/RegisterUser.jsp' /></head></html>");
		}
	   }
   ArrayList<AdoptionQuestion> formQuestions = new ArrayList<AdoptionQuestion>();
   formQuestions =  DbManager.getAdoptionQuestions();
  
   
		
	%>
	 <title>Request Adoption </title>
</head>
<body >

<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="#" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp" id="" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="" ><li>Rescue</li></a>
                    <a href="#" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>
  

 

  <div id="mainContent">

<!--Titulo -->
<div class="containerWidth">
    <h1>Request Adoption for <%out.print(thisPet.getName());%></h1>
    
    <form id = "addNewPetForm" action="${pageContext.request.contextPath}/RegServlet" method="post">
    <h3>Fill out the form below and the the owner will check it later</h3>
    <input type="hidden" name="pagename" value="adoptionRequest"/>
    <input type="hidden" name="reqpetId" value="<%out.print(petId);%>"/>
    <input type="hidden" name="requser" value="<%out.print(user);%>"/>
    <%
    for (int i = 0 ; i < formQuestions.size(); i++ ){
			out.print("<p>"+formQuestions.get(i).getQuestion()+"</p>");
			for (int x = 0 ; x < formQuestions.get(i).getAnswers().size(); x++ ){
				out.print("<input type='radio' name='"+formQuestions.get(i).getAnswers().get(x).getId()+"' value='"+formQuestions.get(i).getAnswers().get(x).getAnswerId()+"'>"+formQuestions.get(i).getAnswers().get(x).getAnswer()+"<br>");
			}
			out.print("<br>");
    }
    
    
    %>
    <input type="submit" class="blueButton" value="submit"/>
    </form>

</div>


</div>
</body>
</html>