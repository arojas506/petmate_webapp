<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>



<!--


ViewPet genera una pagina dado un petIden link como valor paramtreado (ID)


-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/hide.js"> </script>
��

 
  <%@page import="Application.*"%>
  <%@page import="bean.DbManager"%>
  <%@page import="java.util.*"%>
   
   <% 	
   String petId = request.getParameter("id");
   AppPet thisPet  = new AppPet();
   String user = new String();
   String name  = new String();
	String ipAddress =new String();
   if (!DbManager.isPet(petId)){
	   out.print("<meta http-equiv='refresh' content='0; url=../error/error.jsp?error=No pet found' />");
	   
   }else{
	   System.out.println(petId);
	   thisPet = DbManager.getPetProfile(Integer.parseInt(petId));
	   	ipAddress = request.getHeader("X-FORWARDED-FOR");  
		if (ipAddress == null) {  
			ipAddress = request.getRemoteAddr();  
		}
		user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
		if (user.equals("")){
			
				 out.print("<meta http-equiv='refresh' content='0; url=../register/RegisterUser.jsp' />");
		}
	   }
   boolean isFirstOwner = DbManager.isPetsFirstOwner(user,petId);
   boolean noRelatedUser = true;
   if(isFirstOwner) noRelatedUser=false;
   boolean currentOwner = DbManager.isPetsCurrentOwner(user,petId);

	%>
	 <title><%out.print(thisPet.getName());%> @ Petmate</title>
</head>
<body >
<!--


FB


-->
   <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!--


Menu principal


-->
<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="#" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp" id="" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="" ><li>Rescue</li></a>
                    <a href="../stats/" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }   
               
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>
  

 <!--


Contenido principal


-->

  <div id="mainContent">

<!--Titulo -->
<div class="containerWidth">
    <h1><%out.print(thisPet.getName());%><div class="fb-share-button" data-href="http://google.com" data-layout="button_count"></div></h1>
<!--Titulo -->
  <div id="DS1left">
  <%
  
  %>
  <%
  ArrayList<String> imageList =  DbManager.loadPetImageSet(Integer.parseInt(petId));
  if (imageList.size()>0){
	  System.out.println(imageList.get(0));
	  out.print("<img src='../images/"+imageList.get(0)+"' height='233' width='233'  id='profile'/>");
  }else{
	  out.print(" <img src='images/profilepic.jpg' alt='' id='profile'/>");
  }
  %>
  <div id="DS1leftInfo">
   
    
    <!---
	Div que muestra la informacion publica lad izquierdo.
	-->
	<h1>Pet Information</h1>
<table>
	<tr>
	<td> Animal: </td>
	<td> <%out.print(thisPet.getType()); %>
    </td>
    </tr>
    <tr>
	<td> Breed: </td>
	<td> <%out.print(thisPet.getBreed()); %>
    </td>
    </tr>
    <tr>
    <td> Condition: </td>
	<td> <%out.print(thisPet.getCondition()); %>
    </td>
    </tr>
    <tr>
    <td> <% 	out.print("<span class='adopted'>"+thisPet.isAdopted()+"</span>");

    %>  </td>
	<td> 
    </td>
    </tr>
    </table>
      <%
      
      System.out.println("petId: "+petId);
      if (!thisPet.getOwner().equals(user)){
        out.print("<a href='adoptionRequest.jsp?petId="+petId+"'><button class='blueButton' name='adopt'>Adopt</button></a>");
      };
     /*
      if (isFirstOwner && !thisPet.isAdopted() ){
        	 out.print("<a href='editPet.jsp?petId="+petId+"'><button class='blueButton' name='edit'>Edit</button></a>");
        	 out.print("<a href='viewRequests.jsp?petId="+petId+"'><button class='blueButton' name='Candidates'>Requests</button></a>");
        };
      
      if (currentOwner && thisPet.isAdopted() ){
    	  out.print("<a href='returnRequest.jsp?petId="+petId+"'><button class='blueButton' name='return'>Return</button></a>");
    	  out.print("<a href='uploadPic.jsp?petId="+petId+"'><button class='blueButton' name='newPic'>Upload </button></a>");
      }
      
*/
    %>
</div>
	
	
	

  </div>
  <div id="DS1right" >
  <h3>More about this pet</h3>
  <table>
  <tr>
  <td>Posted By: </td>  <td><%out.print(thisPet.getOwner()); %> </td>
  </tr>
  <tr>
  <td>Owned By: </td><td><%out.print(thisPet.getOwner()); %> </td>
  </tr>
  <tr>
  <td>Posted On: </td><td><%out.print(DbManager.getPetDate(Integer.parseInt(petId))); %> </td>
  </tr>
  <tr>
  <td>Size: </td>  <td><%out.print(thisPet.getSize()); %> </td>
  </tr>
  <tr>
  <td>Color 1: </td>  <td><%out.print(thisPet.getColorA()); %> </td>
  </tr>
  <tr>
  <td>Color 2: </td>  <td><%out.print(thisPet.getColorB()); %> </td>
  </tr>
   <tr>
  <td>Energy:</td>   <td><%out.print(thisPet.getEnergy()); %> </td>
  </tr>
   <tr>
  <td>Ease of training: </td>  <td><%out.print(thisPet.getEaseOfTraining()); %> </td>
  </tr>
  <tr>
  <td>Space Required: </td>  <td><%out.print(thisPet.getSpaceRequired()); %> </td>
  </tr>
  
  </table>
      <button id="infoSwitcher" class="blueButton">More info</button>
  </div>
<div id="DS1rightContact" class = "hide" >
<h3> Contact </h3>
   <table>
  <tr>
  <td>Address:</td>
  <td><%out.print(thisPet.getPetContact().getAddress()); %></td>
  </tr>
  <tr>
  <td>City:</td>
  <td><%out.print(thisPet.getPetContact().getCity()); %></td>
  </tr>
  <tr>
  <td>Province:</td>
  <td><%out.print(thisPet.getPetContact().getState()); %></td>
  </tr>
  <tr>
  <td>Phone:</td>
  <td><%out.print(thisPet.getPetContact().getPhone1()); %></td>
  </tr>
  
  
  </table>
<h3> Vet Contact </h3>
   <table>
  <tr>
  <td>Address:</td>
  <td><%out.print(thisPet.getVet().getAddress()); %></td>
  </tr>
  <tr>
  <td>City:</td>
  <td><%out.print(thisPet.getVet().getCity()); %></td>
  </tr>
  <tr>
  <td>Province:</td>
  <td><%out.print(thisPet.getVet().getState()); %></td>
  </tr>
  <tr>
  <td>Phone:</td>
  <td><%out.print(thisPet.getVet().getPhone1()); %></td>
  </tr>
  
  
  </table>
 <button id="infoSwitcher2" class="blueButton">Back</button>
</div>

</div>
</div>
<div id="medicalCondition" class = "blueBg">	
<div class="containerWidth">
 <div class="darkLeftHalfBox">
   	<h1>Medical Condition</h1>
        <table>
          <tr>
            <td><h2>Illness Name</h2></td>
          </tr>
          <%
          ArrayList<String> illness1 = DbManager.getIllList1(Integer.parseInt(petId));
          for (int i = 0; i < illness1.size(); i= i +2){
        	  out.print("<tr><td>"+illness1.get(i)+"</td><td>"+illness1.get(i+1)+"</td></tr>");
        	 
          }
          %>

        </table>
        
     <h2>Treatments</h2>
        <table>
          <% 
          ArrayList<String> medList = DbManager.getIllList2(Integer.parseInt(petId));
          for (int i = 0; i < medList.size(); i++){
        	  out.print("<tr><td>"+medList.get(i)+"</td></tr>");
        	 
          }
          %>
        </table>
        <table>
        <tr>
            <td><h2>Med name</h2></td><td><h2>Amount</h2></td>
          </tr>
          <% 
          ArrayList<String> illness3 = DbManager.getIllList3(Integer.parseInt(petId));
          for (int i = 0; i < illness3.size(); i=i+2){
        	  out.print("<tr><td>"+illness3.get(i)+"</td><td>"+illness3.get(i+1)+"</td></tr>");
        	 
          }
          %>
        </table>
   	
 
    </div>
    <div class="darkRightHalfBox">
   	<h1>History</h1>
   	<%
   	ArrayList<String> history = new ArrayList<String> ();
   	history = DbManager.getPetHistory(Integer.parseInt(petId));
   	out.print("<table>");
   	out.print("<tr><td><h3>Date</h3></td><td><h3>Description</h3></td></tr>");
   	for (int i = 0; i < history.size(); i = i +2){
   		out.print("<tr><td>"+history.get(i)+"</td><td>"+history.get(i+1)+"</td></tr>");
   	}
   	out.print("</table>");
   	%>


   	 
    </div>
</div>

</div>
<div id="medicalCondition" class = "whiteBg">
<div class="containerWidth">
<h1>Other Pet Pictures <%if (thisPet.getOwner().equals(user)) {
	out.print("<form action=\"../RegServlet\" method='post'>");
	out.print("<input type= 'hidden' name='pagename' value='addNewPetPic'>");
	out.print("<input type= 'hidden' name='ipAddress' value='"+ipAddress+"'>");
	out.print("<input type= 'hidden' name='petId' value='"+petId+"'>");
	out.print("<input type= 'submit' value='Add More'>");
}%></h1>
<%
if (imageList.size() > 1){
	for (int i = 0 ; i < imageList.size() ; i++){
	out.print("<img src='../images/"+imageList.get(i)+"'  id='profile' width='500' height='500'/>");
	}
}



%>
</div>

</div>

</div>
</body>
</html>