<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/hide.js"> </script>
  <script type = "text/javascript" src="javascript/updateFields.js"> </script>

 <%
  ArrayList<String> animals = DbManager.getList("animals");
  ArrayList<String> provinces = DbManager.getList("provinces");
  
  %>
  <script type = "text/javascript" >
    $(document).ready(function()
		  {
		      function updateFields()
		      {
		    	  $("#petSubType").attr("value","");
		          var petType = $("#petType").attr("value");
		          <%
		          for (int i = 0; i < animals.size(); i++){
		        	  out.println(" if (petType == \""+animals.get(i)+"\")");
		        	  out.println("{$(\"#petSubType\").attr(\"list\",\""+animals.get(i)+"var\")};");
		        	  
		          }
		          %>
		          
		      };
		      $("#petType").bind("change", updateFields);
		  });
  
  </script>
  
  <script type = "text/javascript" >
    $(document).ready(function()
		  {
		      function updateCities()
		      {
		    	  $("#vetCity").attr("value","");
		          var vetProvince = $("#vetProvince").attr("value");
		          <%
		          for (int i = 0; i < provinces.size(); i++){
		        	  out.println(" if (vetProvince == \""+provinces.get(i)+"\")");
		        	  out.println("{$(\"#vetCity\").attr(\"list\",\""+provinces.get(i)+"var\")};");
		        	  
		          }
		          %>
		          
		      };
		      $("#vetProvince").bind("change", updateCities);
		  });
  
  </script>
   <script type = "text/javascript" >
    $(document).ready(function()
		  {
		      function updateCities()
		      {
		    	  $("#petCity").attr("value","");
		          var petProvince = $("#petProvince").attr("value");
		          <%
		          for (int i = 0; i < provinces.size(); i++){
		        	  out.println(" if (petProvince == \""+provinces.get(i)+"\")");
		        	  out.println("{$(\"#petCity\").attr(\"list\",\""+provinces.get(i)+"var\")};");
		        	  
		          }
		          %>
		          
		      };
		      $("#petProvince").bind("change", updateCities);
		  });
  
  </script>
<%@page import="Application.*"%>
<%@page import="java.util.*"%>
<%@page import="bean.*"%>
 <datalist id="animalTypes">
 <%
 for (int i =0; i<animals.size();i++){
	 out.print("<option value='"+animals.get(i)+"'>");
 }
 out.print(" </datalist>");
 
 
 ArrayList<String> breeds =  new ArrayList<String> ();
 for (int i =0; i<animals.size();i++){
	 out.print("<datalist id='"+animals.get(i)+"var'>");
	 breeds  = DbManager.getList(animals.get(i));
	 for (int c =0; c< breeds.size();c++){
		 out.print("<option value='"+breeds.get(c)+"'>");
	 }
	 out.print(" </datalist>");
 }
 
 

 
 
 %>
  <datalist id="colourlist"><%
		  ArrayList<String> color =  DbManager.getList("colour");
  for (int i =0; i<color.size();i++){
		 out.print("<option value='"+color.get(i)+"'>");
	 }
  %>
  </datalist>


  <datalist id="healthStatList">
<%
		  ArrayList<String> severity =  DbManager.getList("severity");
  for (int i =0; i<severity.size();i++){
		 out.print("<option value='"+severity.get(i)+"'>");
	 }
  %>
  </datalist>


  

  
  
      <datalist id="sizeList">
<%
		  ArrayList<String> size =  DbManager.getList("size");
  for (int i =0; i<size.size();i++){
		 out.print("<option value='"+size.get(i)+"'>");
	 }
  %>
  </datalist>

 

  <title>PetMate Search</title>
<%@page import="Application.*"%>
   <%@page import="bean.DbManager"%>
   <%@page import="java.util.ArrayList"%>
   
   <% 	String ipAddress = request.getHeader("X-FORWARDED-FOR");  
	   	if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
	   	}
	   	String user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
		if (user.equals("")){
			 out.print("<meta http-equiv='refresh' content='0; url=../register/RegisterUser.jsp' />");
		 }
	%>
</head>
<body >


<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="#" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="link-start" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp"  id="link-process" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="link-portfolio" ><li>Rescue</li></a>
                    <a href="#" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>

  <div id="LeftSideMenuBar">
    <div id="LeftSideMenuBarContent">
    <h1 >Search Pets</h1>

    <h3>By Name</h3>
    <form action="search.jsp" method="get">

    <input type="text" name="petname" value="" >
    

    <h3>By Type </h3>

    
    <input  class="dropDown" name="petType" id="petType" list="animalTypes"  >


    <h3>By Breed </h3>
    <input class="dropDown" name="petSubTypex" id="petSubType"  list=""  >
   
    <h3>By Condition</h3>
    <input class="dropDown" name="condition"  list="healthStatList"  >
    <h3>By Size</h3>
     <input class="dropDown" name="size"  list="sizeList"  >
    <h3>By Color</h3>
    <input class="dropDown" name="color"  list="colourlist"  ><br>
        <input type="submit" value="Search" class= "whiteButton">
 </form>
  </div>
  </div>
  <div id="RightSideContent">
  <div id="petResultframe">
   <%
    String pName  = request.getParameter("petname");
    String pType  = request.getParameter("petType");
    String pSubType  = request.getParameter("petSubTypex");
    String pCond = request.getParameter("condition");
    String pColor  = request.getParameter("color");
    String pSize  = request.getParameter("size");
    ArrayList<AppPet> matchingPets = new ArrayList<AppPet>();
    matchingPets = DbManager.getMatchingPets(pName, pType, pSubType, pCond, pSize, pColor);
    for (int i = 0 ; i < matchingPets.size(); i++){
    	AppPet pet = matchingPets.get(i);
    	out.print("<div class='petResult'>");
    	ArrayList<String> imageList =  DbManager.loadPetImageSet(pet.getId());
    	if (imageList.size() > 0)out.print("<img src='../images/"+imageList.get(0)+"'>");
    	else out.print("<img src='images/profilepic.jpg'>");;
    	out.print("<table><tr><td>");
    	out.print("<h3>Name: <a href='viewPet.jsp?id="+pet.getId()+"'>");
    	out.print(pet.getName());out.print("</h3></a></td>");
    	out.print("<td><h3>Owner: <a href='../user/viewuser.jsp?id="+pet.getGiver()+"'>");
    	out.print(pet.getGiver());out.print("</h3></a></td></tr>");
    	out.print("<tr><td><h3>Type: ");
    	out.print(pet.getType());out.print("</h3></td>");
    	out.print("<td><h3>Breed: ");
    	out.print(pet.getBreed());out.print("</h3></td></tr>");
    	out.print("<tr><td><h3>Condition: ");
    	out.print(pet.getCondition());out.print("</h3></td>");
    	out.print("<td><h3>Color: ");
    	out.print(pet.getColorA());out.print("</h3></td></tr>");
    	out.print("<tr><td><h3>Size: ");
    	out.print(pet.getSize());out.print("</h3></td><td></td></tr></table>");
    	out.print("</div>");
    }
    %>
  
  </div>
   
    
</div>
</body>
</html>