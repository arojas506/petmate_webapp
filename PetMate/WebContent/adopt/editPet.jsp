	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/hide.js"> </script>
��

 
  <%@page import="Application.*"%>
  <%@page import="bean.DbManager"%>
  <%@page import="java.util.*"%>
   
   <% 	
   String petId = request.getParameter("petId");
   System.out.println(petId);
   AppPet thisPet  = new AppPet();
   String user = new String();
   String name  = new String();
   if (!DbManager.isPet(petId)){
	   out.print("<meta http-equiv='refresh' content='0; url=../error/usernotfound.jsp' />");
	   
   }else{
	   thisPet = DbManager.getPetProfile(Integer.parseInt(petId));
	   	String ipAddress = request.getHeader("X-FORWARDED-FOR");  
		if (ipAddress == null) {  
			ipAddress = request.getRemoteAddr();  
		}
		user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
		if (user.equals("")){
				 out.print("<meta http-equiv='refresh' content='0; url=../register/RegisterUser.jsp' />");
		}
	   }
   ArrayList<AdoptionQuestion> formQuestions = new ArrayList<AdoptionQuestion>();
   formQuestions =  DbManager.getAdoptionQuestions();
  
   
		
	%>
	 <title>Request Adoption </title>
</head>
<body >

<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="#" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp" id="" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="" ><li>Rescue</li></a>
                    <a href="#" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>
  

 

  <div id="mainContent">

<!--Titulo -->
<div class="containerWidth">
    <h1>Edit info for <%out.print(thisPet.getName());%></h1>
    
    <form id = "addNewPetForm" action="${pageContext.request.contextPath}/RegServlet" method="post">
    <h2>Fill out the form below and the the owner will check it later</h2>
    <input type="hidden" name="pagename" value="editPet"/>
    <input type="hidden" name="pet" value="<%out.print(petId);%>"/>
    <input type="hidden" name="user" value="<%out.print(user);%>"/>
    <table>
    <tr>
    <td>Pet Name</td>
    <td> <input type="text" name="newPetName" value="<%out.print(thisPet.getName());%>"/></td>
    </tr>
    
    <tr>
    <td>Pet Type</td>
    <td> <input type="text" name="newPetType" value="<%out.print(thisPet.getType());%>"/></td>
    </tr>
    
    <tr>
    <td>Pet Breed</td>
    <td> <input type="text" name="newPetBreed" value="<%out.print(thisPet.getBreed());%>"/></td>
    </tr>
    
    
    <tr>
    <td>Gender</td>
    <td> <input type="text" name="newPetGender" value="<%out.print(thisPet.getGender());%>"/></td>
    </tr>
    
    
    <tr>
    <td>Pet Size</td>
    <td> <input type="text" name="newPetSize" value="<%out.print(thisPet.getSize());%>"/></td>
    </tr>
    
    
    <tr>
    <td>Pet Color</td>
    <td> <input type="text" name="newPetColor" value="<%out.print(thisPet.getColorA());%>"/></td>
    </tr>
    
    
    <tr>
    <td>Pet Energy</td>
    <td> <input type="text" name="newPetEnergy" value="<%out.print(thisPet.getEnergy());%>"/></td>
    </tr>
    
    <tr>
    <td>Training Ease</td>
    <td> <input type="text" name="newPetTraining" value="<%out.print(thisPet.getEaseOfTraining());%>"/></td>
    </tr>
    
    <tr>
    <td>Health Status</td>
    <td> <input type="text" name="newHealthStat" value="<%out.print(thisPet.getEaseOfTraining());%>"/></td>
    </tr>
    
    <tr>
    <td>Illness 1</td>
    <td> <input type="text" name="newTreatment1" value="<%//out.print(thisPet.getPetIllnes().get(0).getName());%>"/></td>
    </tr>
     <tr>
    <td>Treatment</td>
    <td> <input type="text" name="newTreatment1" value="<%//out.print(thisPet.getPetIllnes().get(0).getTreatment());%>"/></td>
    <td>Description</td>
    <td> <input type="text" name="newTreatment1" value="<%//out.print(thisPet.getPetIllnes().get(0).getDescription());%>"/></td>
    </tr>
    
    <tr>
    <td>Illness 2</td>
    <td> <input type="text" name="newTreatment2" value="<%//out.print(thisPet.getPetIllnes().get(1).getName());%>"/></td>
    </tr>
     <tr>
    <td>Treatment</td>
    <td> <input type="text" name="newTreatment2" value="<%//out.print(thisPet.getPetIllnes().get(1).getTreatment());%>"/></td>
    <td>Description</td>
    <td> <input type="text" name="newTreatment2" value="<%//out.print(thisPet.getPetIllnes().get(1).getDescription());%>"/></td>
    </tr>
    
    
    <tr>
    <td>Illness 3</td>
    <td> <input type="text" name="newTreatment3" value="<%//out.print(thisPet.getPetIllnes().get(2).getName());%>"/></td>
    </tr>
     <tr>
    <td>Treatment</td>
    <td> <input type="text" name="newTreatment3" value="<%//out.print(thisPet.getPetIllnes().get(2).getTreatment());%>"/></td>
    <td>Description</td>
    <td> <input type="text" name="newTreatment3" value="<%//out.print(thisPet.getPetIllnes().get(2).getDescription());%>"/></td>
    </tr>
    
    <tr>
    <td>Med 1</td>
    <td> <input type="text" name="newMed1" value="<%//out.print(thisPet.getPetDrugs().get(0).getName());%>"/></td>
    <td>Description</td>
    <td> <input type="text" name="newMed1" value="<%//out.print(thisPet.getPetDrugs().get(0).getDescription());%>"/></td>
    </tr>
    <tr>
    <td>Med 2</td>
    <td> <input type="text" name="newMed2" value="<%//out.print(thisPet.getPetDrugs().get(1).getName());%>"/></td>
    <td>Description</td>
    <td> <input type="text" name="newMed2" value="<%//out.print(thisPet.getPetDrugs().get(1).getDescription());%>"/></td>
    </tr>
     <tr>
    <td>Med 2</td>
    <td> <input type="text" name="newMed3" value="<%//out.print(thisPet.getPetDrugs().get(1).getName());%>"/></td>
    <td>Description</td>
    <td> <input type="text" name="newMed2" value="<%//out.print(thisPet.getPetDrugs().get(1).getDescription());%>"/></td>
    </tr>
    
    
    <tr>
    <td>Med 3</td>
    <td> <input type="text" name="newMed3" value="<%//out.print(thisPet.getPetDrugs().get(3).getName());%>"/></td>
    <td>Description</td>
    <td> <input type="text" name="newMed3" value="<%//out.print(thisPet.getPetDrugs().get(3).getDescription());%>"/></td>
    </tr>
    
    <tr>
    <td> </td>
    <td> <input type="text" name="newProvince" value="<%//out.print(thisPet);%>"/></td>
    <td>Description</td>
    <td> <input type="text" name="newMed3" value="<%//out.print(thisPet.getPetDrugs().get(2).getDescription());%>"/></td>
    </tr>
    
    </table>
    <input type="submit" class="blueButton" value="Save"/>
    </form>

</div>


</div>
</body>
</html>