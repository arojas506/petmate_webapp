<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link rel="stylesheet" href="index.css"  type="text/css" media="screen">
<%@page import="Application.AppSession"%>
   <%@page import="bean.DbManager"%>
   
   <% 	
   String msg = request.getParameter("msg");
   String ipAddress = request.getHeader("X-FORWARDED-FOR");  
	   	if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
	   	}
	   	String user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());

		if (DbManager.isAdmin(user) && !user.equals("")){
    		out.print("<meta http-equiv='refresh' content='0; url=../admin/adminDashboard.jsp' />");
    		
    	}
		 if (!user.equals("")){
			out.print("<meta http-equiv='refresh' content='0; url=../user/dashboard.jsp' />");
		}
	%>
</head>
<body >



<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="#" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="link-start" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp" id="link-process" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="link-portfolio" ><li>Rescue</li></a>
                    <a href="#" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>
<div class="container">

  <div id="login-form">
	<h1><%
if (msg != null)
out.print(msg); %></h1>
    <h3>Log in</h3>

    <fieldset>

      <form action="${pageContext.request.contextPath}/RegServlet" method="post">
      <input type="hidden" name="pagename" value="login"/>

        <input type="text" name="Username" value="Username" onBlur="if(this.value=='')this.value='Username'" onFocus="if(this.value=='Username')this.value='' "> 

        <input type="password" name="Password" value="Password" onBlur="if(this.value=='')this.value='Password'" onFocus="if(this.value=='Password')this.value='' "> 
        <p>Not a member yet? <a href="../register/RegisterUser.jsp" id="register">Sign up!</a></p>
        <input type="submit" value="Enter">

      </form>

    </fieldset>
    
  </div> 
 </div>
</body>
</html>