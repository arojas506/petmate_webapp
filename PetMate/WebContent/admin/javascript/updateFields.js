$(document).ready(function()
{
    function updateFields()
    {
        var petType = $("#petType").attr("value");
        if (petType == "Dog"){
        	$("#petSubType").attr("list","dogvar");
        };
        if (petType == "Cat"){
        	$("#petSubType").attr("list","catvar");
        }
        
    };
    $('input').bind("change", updateFields);
});