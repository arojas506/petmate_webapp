<!DOCTYPE html PUhBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/hide.js"> </script>
   
  <title>Admin Dashboard</title>
  <%@page import="Application.*"%>
   <%@page import="bean.DbManager"%>
   
   <%  
   String user = "";
   AppUser viewUser = new AppUser();
   String msg = request.getParameter("mssg");
   String ipAddress = request.getHeader("X-FORWARDED-FOR");  
     	if (ipAddress == null) {  
       		ipAddress = request.getRemoteAddr();  
      	}
  user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
    	if (user.equals("")){
       		out.print("<meta http-equiv='refresh' content='0; url=../register/RegisterUser.jsp' />");
     	}
    	if (!DbManager.isAdmin(user)){
    		out.print("<meta http-equiv='refresh' content='0; url=../error/error.jsp?error=Invalid Access' />");
    		
    	}
    	
    	 viewUser = DbManager.getUserProfile(user);
    	 
 

    	
  
  %>
  
</head>
<body >


<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="#" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="link-start" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp" id="link-process" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="link-portfolio" ><li>Rescue</li></a>
                    <a href="#" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>
  

 

  <div id="mainContent">

<!--Titulo -->
<div id="DashboardSection1" class="containerWidth">
    <h1>Admin Dashboard</h1>
<!--Titulo -->
  <div id="DS1left">
   <%String url = DbManager.getUserProfilePicture(viewUser.getUserName());
  if (!url.equals("no_image")){
	  out.print("<img src='../images/"+url+"' height='233' width='233'  id='profile'/>");
  }else{
	  out.print(" <img src='images/profilepic.jpg' alt='' id='profile'/>");
  }
  %>
  
  <div id="DS1leftInfo">
   
    
    <!---
	Div que muestra la informacion publica lad izquierdo.
	-->
    <h2> Public Info</h2>
    <table>
    <tr>
    	<td>Username</td>
    	<td><%out.print(viewUser.getUserName()); %></td>
	</tr>
	<tr>
	<td> Name</td>
	<td> <%out.print(viewUser.getUserFullName()); %>
    </td>
    </tr>
    <tr><td> Email</td>
    	<td><% out.print(viewUser.getUserEmail()); %>
    	</td>
    </tr>
    <tr><td> Blacklisted</td>
    	<td> <%if(DbManager.isBlacklisted(viewUser.getUserId())) out.print("YES");
    	else out.print("NO");
    	%>
    	</td>
    </tr>
    </table>
    <table id="buttnHolder">
    <tr><td>
		<form action="${pageContext.request.contextPath}/RegServlet" method="post"> 
		<input type="hidden" name="pagename" value="logout"/>
		<input type = "hidden" value="<%out.print(user);%>" name="Username"/>
		<input type = "hidden" value="<%out.print(ipAddress);%>" name="Ip"/>
		<input type="submit" id="logout" value="Log Out" class="blueButton"/>
		</form></td>
		
		<td>
		<button id="editBasicInfo"  class="blueButton">Edit Info</button></td>
	</table>
		</div>
	
	
	
	<!---
	Div Para cambiar la informacion personal Basica
	-->
	
	 <div id="DS1leftEdit" class="hide">
    	<h2> Edit Info</h2>
    	<form action="${pageContext.request.contextPath}/RegServlet" method="post">
    		<input type="hidden" name="pagename" value="updateUserBasicInfo"/>
    		<table>
    			<tr>
    			<td>Username</td>
    			<td><% out.print(user); %></td>
			</tr>
			<tr>
			<td></td>
			<td> <input type="hidden" name="userName" value="<%out.print(viewUser.getUserName());%>"/>
   		 </td>
    	</tr>
		<tr>
		<td> Name</td>
		<td> <input type="text" name="userNewFullName" value="<%out.print(viewUser.getUserFullName()); %>"/>
   		 </td>
    	</tr>
    	<tr><td> Email</td>
    	<td> <input type="text" name="userNewEmail" value="<% out.print(viewUser.getUserEmail()); %>"/>
    	</td>
    </tr>
    <tr><td> Password</td>
    	<td> <input type="password" name="userNewPass" value=""/>
    	</td>
    </tr>
    </table>
    
    <input type="submit" id="SaveChanges" value="Save" class="blueButton" >
    </form>
    <button id="cancelbttn" class="blueButton" >Cancel</button>
    

	</div>
	
	
	 <a href="../picture/upload.jsp?type=user&val=<%out.print(user);%>" ><button id="addPic"  class="blueButton">Change Pic</button></a>
	<!---
	Div de la informacin de contacto
	-->	
	
	
  </div>
  <div id="DS1right">
  <h1>Contact Info</h1>
    <table>
  <tr>
  <td>Address:</td>
  <td><%out.print(viewUser.getAddress()); %></td>
  </tr>
  <tr>
  <td>City:</td>
  <td><%out.print(viewUser.getCity()); %></td>
  </tr>
  <tr>
  <td>Province:</td>
  <td><%out.print(viewUser.getProvince()); %></td>
  </tr>
  <tr>
  <td>Phone:</td>
  <td><%out.print(viewUser.getPhone()); %></td>
  </tr>
  
  </table>

  </div>


</div>
<div class = "blueBg" id="adoptions">
  <div class = "containerWidth">
    <h1>Pet </h1>
    <table class="adminTableDisplay">
      <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="addAnimalName"/>
        <td>New Animal</td>
        <td><input type="text" name="newAnimalName" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Add"/></td>
        </form>
       </tr>

        <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="deleteAnimalName"/>
        <td>Delete Animal</td>
        <td><input type="text" name="deleteAnimalName" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Delete"/></td>
        </form>
       </tr>

       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="addBreed"/>
        <td>Matching Animal</td>
        <td><input type="text" name="matchinganimalName" value=""/></td>
        <td>Add Breed</td>
        <td><input type="text" name="animalbreedName" value=""/></td>
        <td><input type="submit" class = "whiteButton" value="Add"/></td>
        </form>
       </tr>
       
       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="deleteBreed"/>
        <td>Delete Breed ID </td>
        <td><input type="text" name="animalbreedID" value=""/></td>
        <td>Delete Breed</td>
        <td><input type="text" name="animalbreedName" value=""/></td>
        <td><input type="submit" class = "whiteButton" value="Delete"/></td>
        </form>
       </tr>

       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="addColor"/>
        <td>New Color</td>
        <td><input type="text" name="newColor" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Add"/></td>
        </form>
       </tr>
       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="deleteColor"/>
        <td>Delete Color by ID</td>
        <td><input type="text" name="deleteColor" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Delete"/></td>
        </form>
       </tr>

       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="addEnergy"/>
        <td>New Pet Energy</td>
        <td><input type="text" name="newEnergyLevel" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Add"/></td>
        </form>
       </tr>

       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="deleteEnergy"/>
        <td>Delete Pet Energy</td>
        <td><input type="text" name="deleteEnergyLevel" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Delete"/></td>
        </form>
       </tr>
       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="addTraining"/>
        <td>New Training level</td>
        <td><input type="text" name="trainingLevel" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Add"/></td>
        </form>
       </tr>

       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
         <input type="hidden" name="pagename" value="deleteTraining"/>
        <td>Delete Training level</td>
        <td><input type="text" name="deleteTraining" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Delete"/></td>
        </form>
       </tr>

      <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
         <input type="hidden" name="pagename" value="addPetIllNess"/>
        <td>Add pet illness</td>
        <td><input type="text" name="addPetIllnes" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Add"/></td>
        </form>
       </tr>

      <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="deletePetIllNess"/>
        <td>Delete pet illness</td>
        <td><input type="text" name="deletePetIllNess" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Delete"/></td>
        </form>
       </tr>

      <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="addTreatment"/>
        <td>Matching pet illness</td>
        <td><input type="text" name="matchIlness" value=""/></td>
        <td>Add Treatment</td>
        <td><input type="text" name="addTreatmentName" value=""/></td>
        <td><input type="submit" class = "whiteButton" value="Add"/></td>
        </form>
       </tr>
       
       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="deleteTreatment"/>
        <td>Delete Treatment by ID</td>
        <td><input type="text" name="treatmentID" value=""></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Delete"/></td>
        </form>
       </tr>

       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="addDrug"/>
        <td>Add Drug Name</td>
        <td><input type="text" name="drugName" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Add"/></td>
        </form>
       </tr>
       
       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="deleteDrugName"/>
        <td>Delete Drug Name</td>
        <td><input type="text" name="drugName" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "whiteButton" value="Delete"/></td>
        </form>
       </tr>

    </table>
    

  </div>
</div>
<div class = "whiteBg" id="">
  <div class = "containerWidth">
    <h1>Adoption  </h1>
    
    
    <h2>Adoption Tests</h2>
    <table class="adminTableDisplay">
    <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="addQuestionAdoptionTest"/>
        <td>Add Question</td>
        <td><input type="text" name="newQuestion" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "blueButton" value="Add"/></td>
        </form>
       </tr>
      <tr>
      
      
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="deleteQuestionAdoptionTest"/>
        <td>Delete Question by ID</td>
        <td><input type="text" name="questionID" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "blueButton" value="Delete"/></td>
        </form>
       </tr>
       
       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="addAnswerAdoptionTest"/>
        <td>Matching Question ID</td>
        <td><input type="text" name="matchQuestionID" value=""/></td>
        <td>Add Answer</td>
        <td><input type="text" name="newAnswer" value=""/></td>
        <td><input type="submit" class = "blueButton" value="Add"/></td>
        </form>
       </tr>
       
       <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="deleteAnswerAdoptionTest"/>
        <td>Delete Answer by ID</td>
        <td><input type="text" name="answerId" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "blueButton" value="Delete"/></td>
        </form>
       </tr>
    </table>
    
    
    
    
    <h2>Find me a pet Tests</h2>
    <table class="adminTableDisplay">
    <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="addQuestionFindPet"/>
        <td>Add Question</td>
        <td><input type="text" name="newQuestion" value=""/></td>
        <td>Mathing atribute</td>
        <td><input type="text" name="matchingAtrr" value=""/></td>
        <td><input type="submit" class = "blueButton" value="Add"/></td>
        </form>
       </tr>
       
       
      <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="deleteQuestionFindPet"/>
        <td>Delete Question by ID</td>
        <td><input type="text" name="questionID" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "blueButton" value="Delete"/></td>
        </form>
       </tr>

    </table>
    
    
     <h2>Home Page settings</h2>
     <h3>Type the number of months for the pets that have priority on the home page </h3>
      <table class="adminTableDisplay">
    <tr>
        <form action="${pageContext.request.contextPath}/RegServlet" method="post">
        <input type="hidden" name="pagename" value="configureHomePage"/>
        <td>Number of months</td>
        <td><input type="text" name="monthsOld" value=""/></td>
        <td></td>
        <td></td>
        <td><input type="submit" class = "blueButton" value="Change"/></td>
        </form>
       </tr>

    </table>
  </div>


</div>
</body>
</html>