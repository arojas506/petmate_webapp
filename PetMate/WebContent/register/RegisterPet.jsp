<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="RegisterPet.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/hide.js"> </script>
  <%
  ArrayList<String> animals = DbManager.getList("animals");
  ArrayList<String> provinces = DbManager.getList("provinces");
  
  %>
  <script type = "text/javascript" >
    $(document).ready(function()
		  {
		      function updateFields()
		      {
		    	  $("#petSubType").attr("value","");
		          var petType = $("#petType").attr("value");
		          <%
		          for (int i = 0; i < animals.size(); i++){
		        	  out.println(" if (petType == \""+animals.get(i)+"\")");
		        	  out.println("{$(\"#petSubType\").attr(\"list\",\""+animals.get(i)+"var\")};");
		        	  
		          }
		          %>
		          
		      };
		      $("#petType").bind("change", updateFields);
		  });
  
  </script>
  
  <script type = "text/javascript" >
    $(document).ready(function()
		  {
		      function updateCities()
		      {
		    	  $("#vetCity").attr("value","");
		          var vetProvince = $("#vetProvince").attr("value");
		          <%
		          for (int i = 0; i < provinces.size(); i++){
		        	  out.println(" if (vetProvince == \""+provinces.get(i)+"\")");
		        	  out.println("{$(\"#vetCity\").attr(\"list\",\""+provinces.get(i)+"var\")};");
		        	  
		          }
		          %>
		          
		      };
		      $("#vetProvince").bind("change", updateCities);
		  });
  
  </script>
   <script type = "text/javascript" >
    $(document).ready(function()
		  {
		      function updateCities()
		      {
		    	  $("#petCity").attr("value","");
		          var petProvince = $("#petProvince").attr("value");
		          <%
		          for (int i = 0; i < provinces.size(); i++){
		        	  out.println(" if (petProvince == \""+provinces.get(i)+"\")");
		        	  out.println("{$(\"#petCity\").attr(\"list\",\""+provinces.get(i)+"var\")};");
		        	  
		          }
		          %>
		          
		      };
		      $("#petProvince").bind("change", updateCities);
		  });
  
  </script>
<%@page import="Application.*"%>
<%@page import="java.util.*"%>
<%@page import="bean.*"%>
 <datalist id="animalTypes">
 <%
 for (int i =0; i<animals.size();i++){
	 out.print("<option value='"+animals.get(i)+"'>");
 }
 out.print(" </datalist>");
 
 
 ArrayList<String> breeds =  new ArrayList<String> ();
 for (int i =0; i<animals.size();i++){
	 out.print("<datalist id='"+animals.get(i)+"var'>");
	 breeds  = DbManager.getList(animals.get(i));
	 for (int c =0; c< breeds.size();c++){
		 out.print("<option value='"+breeds.get(c)+"'>");
	 }
	 out.print(" </datalist>");
 }
 
 
 ArrayList<String> cities =  new ArrayList<String> ();
 for (int i =0; i<provinces.size();i++){
	 out.print("<datalist id='"+provinces.get(i)+"var'>");
	 cities  = DbManager.getCitiesList(provinces.get(i));
	 for (int c =0; c< cities.size();c++){
		 out.print("<option value='"+cities.get(c)+"'>");
	 }
	 out.print(" </datalist>");
 }
 
 
 
 %>
  <datalist id="colourlist"><%
		  ArrayList<String> color =  DbManager.getList("colour");
  for (int i =0; i<color.size();i++){
		 out.print("<option value='"+color.get(i)+"'>");
	 }
  %>
  </datalist>


  <datalist id="healthStatList">
<%
		  ArrayList<String> severity =  DbManager.getList("severity");
  for (int i =0; i<severity.size();i++){
		 out.print("<option value='"+severity.get(i)+"'>");
	 }
  %>
  </datalist>

  <datalist id="illness">
<%
		  ArrayList<String> sickness =  DbManager.getList("illness");
  for (int i =0; i<sickness.size();i++){
		 out.print("<option value='"+sickness.get(i)+"'>");
	 }
  %>
  </datalist>
  
    <datalist id="energylist">
<%
		  ArrayList<String> energy =  DbManager.getList("energy");
  for (int i =0; i<energy.size();i++){
		 out.print("<option value='"+energy.get(i)+"'>");
	 }
  %>
  </datalist>
  
  
      <datalist id="sizeList">
<%
		  ArrayList<String> size =  DbManager.getList("size");
  for (int i =0; i<size.size();i++){
		 out.print("<option value='"+size.get(i)+"'>");
	 }
  %>
  </datalist>
  
        <datalist id="trainList">
<%
		  ArrayList<String> train =  DbManager.getList("train");
  for (int i =0; i<train.size();i++){
		 out.print("<option value='"+train.get(i)+"'>");
	 }
  %>
  </datalist>
  
          <datalist id="spaceList">
<%
		  ArrayList<String> space =  DbManager.getList("space");
  for (int i =0; i<space.size();i++){
		 out.print("<option value='"+space.get(i)+"'>");
	 }
  %>
  </datalist>
  
            <datalist id="petMeds">
<%
		  ArrayList<String> med =  DbManager.getList("medicament");
  for (int i =0; i<med.size();i++){
		 out.print("<option value='"+med.get(i)+"'>");
	 }
  %>
  </datalist>
              <datalist id="illnesses">
<%
		  ArrayList<String> illness =  DbManager.getList("illness");
  for (int i =0; i<illness.size();i++){
		 out.print("<option value='"+illness.get(i)+"'>");
	 }
  %>
  </datalist>
  
                <datalist id="treatments">
<%
		  ArrayList<String> treatments =  DbManager.getList("treatment");
  for (int i =0; i<treatments.size();i++){
		 out.print("<option value='"+treatments.get(i)+"'>");
	 }
  %>
  </datalist>
  
 <datalist id="provinces">
<%
  for (int i =0; i<provinces.size();i++){
		 out.print("<option value='"+provinces.get(i)+"'>");
	 }
  %>
  </datalist>


   <datalist id="gender">
  <option value="Male">
  <option value="Female">
  </datalist>

  <title>Register Pet</title>
<%@page import="Application.AppSession"%>
   <%@page import="bean.DbManager"%>
   
   <% 	String ipAddress = request.getHeader("X-FORWARDED-FOR");  
	   	if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
	   	}
	   	String user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());

	   	if (user.equals("")){
			 out.print("<meta http-equiv='refresh' content='0; url=../register/RegisterUser.jsp' />");
	}
	%>
</head>
<body >



<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="../" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="link-start" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp" id="link-process" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="link-portfolio" ><li>Rescue</li></a>
                    <a href="../stats/" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>

  <div id="LeftSideMenuBar">
    <div id="LeftSideMenuBarContent">
    <h1 >Add Pet</h1>

    <h2 id="st1" class="stepCurrent">Add Basic Info</h2>
    <h2 id="st2">Medical Situation</h2>
    <h2 id="st3">Contact info</h2>
    <h2 id="st4">Done!</h2>

  </div>
  </div>
  <div id="RightSideContent">
    <form id ="addNewPetForm" action="${pageContext.request.contextPath}/RegServlet" method="post">
    <input type="hidden" name="pagename" value="registerPet"/>
    
      <div id="DivForm1" class="subform">
        <h3>Step 1: Add basic Info</h3>
        <input  type="hidden" name="ipAddress" value="<%out.print(ipAddress);%>" required>
        <input  type="hidden" name="owner" value="<%out.print(user); %>" required >
        <table>
          <tr>
            <td>Pet Type</td>
            <td><input  class="dropDown" name="petType" id="petType" list="animalTypes" required ></td>
          </tr>
          <tr>
            <td>Pet Type Variation</td>
            <td><input class="dropDown" name="petSubType" id="petSubType"  list=""  required></td>
          </tr>
          <tr>
            <td>Pet Name</td>
            <td><input type="text" name="petName" id="petName" required></td>
          </tr>
          <tr>
            <td>Gender</td>
            <td><input class="dropDown" name="petGender" id="petGender"  list="gender"  required></td>
          </tr>
          <tr>
            <td>Pet Color 1</td>
            <td><input class="dropDown" name="petColor1" id="colour"  list="colourlist"  required></td>
          </tr>
          <tr>
            <td>Pet Color 2</td>
            <td><input class="dropDown" name="petColor2" id="colour"  list="colourlist" ></td>
          </tr>
          <tr>
            <td>Pet Energy</td>
            <td><input class="dropDown" name="petEnergy" id="petEnergy" list="energylist"  required></td>
          </tr>
          <tr>
            <td>Pet size</td>
            <td><input class="dropDown" name="petSize" id="petSize" list="sizeList"  required></td>
          </tr>
          <tr>
            <td>Ease of training</td>
            <td><input class="dropDown" name="petTrain" id="trainList" list="trainList" required></td>
          </tr>
          <tr>
            <td>Space Required</td>
            <td><input type="text" name="petSpace" id="spaceList" list="spaceList" required></td>
          </tr>
          <tr>
            <td>How did you find it?</td>
            <td><input type="text" name="petAbandonDesc" id="petAbandonDesc" required></td>
          </tr>



        </table>

      </div>

      <div id="DivForm2" class="hide"> 
        <h3><span class="FormTitle">Step 3: About the pet's medical status</span></h3>
        <table>
          <tr>
            <td>Pet Health status</td>
            <td><input  class="dropDown" name="petHealthStatus" id="petType" list="healthStatList" required ></td>
          </tr>
        </table>
        <h2>Pet Illnesses</h2>
        <table>
          <tr>
            <td>Ilness</td>
            <td>Date</td>
          </tr>
          <tr>
            <td><input  class="dropDown" name="petHealthIllnes1" id="petIllness1" list="illnesses"  ></td>
         
            <td><input  type="date" name="petHealthIllnesDesc1" id="petType"  ></td>
          </tr>

          <tr>
            <td><input  class="dropDown" name="petHealthIllnes2" id="petIllness2" list="illnesses"  ></td>
           
            <td><input  type="date" name="petHealthIllnesDesc2" id="petType"   ></td>
          </tr>
          <tr>
            <td><input  class="dropDown" name="petHealthIllnes3" id="petIllness3" list="illnesses"  ></td>

            <td><input  type="date" name="petHealthIllnesDesc3" id="petType"   ></td>
          </tr>



        </table>
		<p>Posible treatments will be shown on the pet's profile</p>

      </div>
      <div id="DivForm3" class="hide">
        <h3><span class="FormTitle">Step 4: Provide Contact information</span></h3>
  		 <h2>Contact Info</h2>
        <table>
          <tr>
            <td>Name </td>
            <td><input  type="text" name="peContactName" id="petType" required></td>
          </tr>
          <tr>
            <td>Email</td>
            <td><input  type="text" name="petEmail" id="petType"   required></td>
          </tr>
          <tr>
            <td>Phone</td>
            <td><input  type="text" name="petPhone" id="petType"  required ></td>
          </tr>
          <tr>
            <td>Province</td>
            <td><input  class="dropDown" name="petProvince" id="petProvince" list="provinces"  required></td>
          </tr>
          <tr>
            <td>City</td>
            <td><input  class="dropDown" name="petCity" id="petCity" list="" required></td>
          </tr>
          <tr>
            <td>Adress</td>
            <td><input  type="text" name="petAddress" id="petAddress"  required></td>
          </tr>
</table>
          
        <h2>Vet contact Info</h2>
        <table>
          <tr>
            <td>Name </td>
            <td><input  type="text" name="vetName" id="petType" required></td>
          </tr>
          <tr>
            <td>Email</td>
            <td><input  type="text" name="vetEmail" id="petType"   required></td>
          </tr>
          <tr>
            <td>Phone</td>
            <td><input  type="text" name="vetPhone" id="petType"  required ></td>
          </tr>
          <tr>
            <td>Province</td>
            <td><input  class="dropDown" name="vetProvince" id="vetProvince" list="provinces" required ></td>
          </tr>
          <tr>
            <td>City</td>
            <td><input  class="dropDown" name="vetCity" id="vetCity" list="" required ></td>
          </tr>
          <tr>
            <td>Adress</td>
            <td><input  type="text" name="vetAddress" id="vetAddress" required ></td>
          </tr>
        </table>

      </div>
      <div id="DivForm4" class="hide">
        <h3><span class="FormTitle">Step 5: All set!</span></h3>
        Want to add anything else?
        <textarea name="extrainfo" cols="25" rows="5" style="margin: 0px; width: 599px; height: 202px;">        </textarea><br>
        <input type="submit" class="blueButton" value="Finish!"> 
      </div>
    </form>
    <button type="button" id="prevSubForm" class="hide" >Back</button>
    <button type="button" id="nextSubForm" class="blueButton" >Next</button>
    
</div>
</body>
</html>