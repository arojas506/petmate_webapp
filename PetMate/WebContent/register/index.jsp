<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>
<link rel="stylesheet" href="index.css"  type="text/css" media="screen">
<%@page import="Application.AppSession"%>
   <%@page import="bean.DbManager"%>
   
   <% 
   String msg = request.getParameter("msg");
   String ipAddress = request.getHeader("X-FORWARDED-FOR");  
	   	if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
	   	}
	   	String user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
	   	
	%>
</head>
<body >



<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="../" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="link-start" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp" id="link-process" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="link-portfolio" ><li>Rescue</li></a>
                    <a href="../stats/" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>

	<div class="container">
<h1><%
if (msg != null)
out.print(msg); %></h1>
  <div id="login-form">

    <h3>Register</h3>

    <fieldset>
	
     <form action="${pageContext.request.contextPath}/RegServlet" method="post">
     <a>All values are required</a>
      	<input type="hidden" name="pagename" value="register"/>
        <input type="text" name="regUserName" value="Username" maxlength="10" onBlur="if(this.value=='')this.value='Username'" onFocus="if(this.value=='Username')this.value='' " required><br>
        <input type="text" name="regUserFullName" value="Full Name" maxlength="30"onBlur="if(this.value=='')this.value='Full Name'" onFocus="if(this.value=='Full Name')this.value='' "required><br>
        <input type="text" name="regUserEmail" value="Email" maxlength="20"onBlur="if(this.value=='')this.value='Email'" onFocus="if(this.value=='Email')this.value='' "required><br>
        <input type="password" name="regUserPass" value="Contraseņa" maxlength="10" onBlur="if(this.value=='')this.value='Contraseņa'" onFocus="if(this.value=='Contraseņa')this.value='' "required> 
        <br><input type="submit" value="Create Account">

      </form>

    </fieldset>
    
  </div> <!-- end login-form -->

</div>
</body>
</html>