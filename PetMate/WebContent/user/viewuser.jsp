<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/hide.js"> </script>
��
   
 
  <%@page import="Application.*"%>
  <%@page import="bean.DbManager"%>
  <%@page import="java.util.*"%>
   
   <% 	
   String requestUserProfile = request.getParameter("id");
   AppUser viewUser = new AppUser();
   String user = new String();
   String name  = new String();
   if (DbManager.usernameExists(requestUserProfile)==0){
	   
	   out.print("<meta http-equiv='refresh' content='0; url=../error/error.jsp?error=User not found' />");
	   
   }else{
	  if( DbManager.usernameExists(requestUserProfile)==1){
	    viewUser = DbManager.getUserProfile(requestUserProfile);
	   	String ipAddress = request.getHeader("X-FORWARDED-FOR");  
		if (ipAddress == null) {  
			ipAddress = request.getRemoteAddr();  
		}
		user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
		if (user.equals("")){
			 out.print("<meta http-equiv='refresh' content='0; url=../register/RegisterUser.jsp' />");
	}
		if (user.equals(requestUserProfile)){
				out.print("<meta http-equiv='refresh' content='0; url=../user/dashboard.jsp' />");
		}
	   }
   }if (DbManager.usernameExists(requestUserProfile)==0){
	   out.print("<meta http-equiv='refresh' content='0; url=../error/error.jsp?error=User Not Found' />");
   }
		
	%>
	 <title><%out.print(viewUser.getUserName());%> @ Petmate</title>
</head>
<body >


<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="../" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp" id="" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="" ><li>Rescue</li></a>
                    <a href="../stats/" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>
  

 

  <div id="mainContent">

<!--Titulo -->
<div class="containerWidth">
    <h1><%out.print(viewUser.getUserName());%></h1>
<!--Titulo -->
  <div id="DS1left">
   <%String url = DbManager.getUserProfilePicture(viewUser.getUserName());
  if (!url.equals("no_image")){
	  out.print("<img src='../images/"+url+"' height='233' width='233'  id='profile'/>");
  }else{
	  out.print(" <img src='images/profilepic.jpg' alt='' id='profile'/>");
  }
  %>
  <div id="DS1leftInfo">
   
    
    <!---
	Div que muestra la informacion publica lad izquierdo.
	-->
	<h1>User Information</h1>
    <table>
	<tr>
	<td> Name</td>
	<td> <%out.print(viewUser.getUserFullName()); %>
    </td>
    </tr>
    <tr><td> Email</td>
    	<td ><a href="sendEmail.jsp?target=<% out.print(viewUser.getUserEmail()); %>"><span class="email"><%    	out.print(viewUser.getUserEmail()); %></span></a>
    	</td>
    </tr>
    <tr><td> Blacklisted</td>
    	<td> <%if(DbManager.isBlacklisted(viewUser.getUserId())) out.print("YES");
    	else out.print("NO");
    	%>
    	</td>
    </tr>
    </table>
   <a href="reportuser.jsp?target=<%out.print(viewUser.getUserName()); %>"> <button  class="blueButton">Report</button></a>
</div>
	
	
	

  </div>
  <div id="DS1right">
    <h4> No contact info found this user</h4>
  </div>


</div>
<div class = "blueBg" id="adoptions">
  <div class = "containerWidth">
    <h1><%out.print(viewUser.getUserName());%>'s Pets</h1>
    <div class="darkLeftHalfBox">
   	<h2>On adoption</h2>
   	<%if(!viewUser.hasOnAdoption()){
   		out.print("<h3>This user doesnt have any pets on adoption.</h3>");
   	}
   	else{
   		//Se debe hacer el diplay de todas las adopciones
   	}%> 
    </div>
    <div class="darkRightHalfBox">
   	<h2>Adopted</h2>
   		<%if(!viewUser.hasAdopted()){
   		out.print("<h3>This user hasn't adopted any pet yet.</h3>");
   	}
   	else{
   		//Se debe hacer el diplay de todas las adopciones
   	}%> 
    </div>

  </div>
</div>
<div class = "whiteBg" id="messages">
<div class= "containerWidth"><h1>Comments for <%out.print(viewUser.getUserName());%></h1>
<% if ( DbManager.hasRelation(viewUser.getUserName(), user)) {
	out.print("<button id='commentEnabler' class='blueButton'>Write One</button></a> ");};
	%>
<div id='commentbar' class='hide'><form  action='${pageContext.request.contextPath}/RegServlet' method='post'>
<input type='hidden' name='pagename' value='addComment'/>
<input type='hidden' name='writer'value="<%out.print(user);%>"  required> 
<input type='hidden' name='petID'value="1"  required> 
<input type='hidden' name='target' value="<%out.print(viewUser.getUserName());%>" required><br>
<textarea name='commentValue' cols='25' rows='5' style='margin: 0px; width: 599px; height: 202px;'required></textarea><br>
<p>Score [1-5]: <input type='number' name="score" min="1" max="5" class='blueButton' value="5" required> </p>
<input type='submit' class='blueButton' value='Add'> 
</form></div>
	
<%
ArrayList<Comment>  commentList = DbManager.getComments(requestUserProfile);
for (int i=0; i < commentList.size(); i++){
	Comment thiscomment = new Comment();
	thiscomment  = commentList.get(i);
	out.print("<div class='profileComment'>");
	out.print("<h3><a href='../user/viewuser.jsp?id="+thiscomment.getOrigin()+"'>"+thiscomment.getOrigin()+"</a> "+thiscomment.getDate()+"</h3>");
	out.print("<p>"+thiscomment.getContent()+"</p><br>");
	out.print("<p>Score Obtained: "+thiscomment.getScore()+"</p><br>");
	out.print("</div>");
}
	


%>



</div>

 </div>
</div>
</body>
</html>