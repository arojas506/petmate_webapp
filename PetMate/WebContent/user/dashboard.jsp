<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/hide.js"> </script>
��
   
  <title>PetMate Dashboard</title>
  <%@page import="Application.*"%>
   <%@page import="bean.DbManager"%>
   
   <% 	String ipAddress = request.getHeader("X-FORWARDED-FOR");
   		AppUser viewUser = new AppUser();
	   	if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
	   	}
	   	String user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());
	   	if (user.equals("")){
			 out.print("<meta http-equiv='refresh' content='0; url=../register/RegisterUser.jsp' />");
	}

		viewUser = DbManager.getUserProfile(user);

			
		
	%>
</head>
<body >


<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="../" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="link-start" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp" id="link-process" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="link-portfolio" ><li>Rescue</li></a>
                    <a href="../stats/" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }   
               if (user.equals("")){
   				response.sendRedirect("../index.jsp");
   			}
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>
  

 

  <div id="mainContent">

<!--Titulo -->
<div id="DashboardSection1" class="containerWidth">
    <h1>Dashboard</h1>
<!--Titulo -->
  <div id="DS1left">
   <%String url = DbManager.getUserProfilePicture(viewUser.getUserName());
  if (!url.equals("no_image")){
	  out.print("<img src='../images/"+url+"' height='233' width='233'  id='profile'/>");
  }else{
	  out.print(" <img src='images/profilepic.jpg' alt='' id='profile'/>");
  }
  %>
  
  <div id="DS1leftInfo">
   
    
    <!---
	Div que muestra la informacion publica lad izquierdo.
	-->
    <h2> Public Info</h2>
    <table>
    <tr>
    	<td>Username</td>
    	<td><%out.print(viewUser.getUserName()); %></td>
	</tr>
	<tr>
	<td> Name</td>
	<td> <%out.print(viewUser.getUserFullName()); %>
    </td>
    </tr>
    <tr><td> Email</td>
    	<td><% out.print(viewUser.getUserEmail()); %>
    	</td>
    </tr>
    <tr><td> Blacklisted</td>
    	<td><%if(DbManager.isBlacklisted(viewUser.getUserId())) out.print("YES");
    	else out.print("NO");
    	%>
    	</td>
    </tr>
    </table>
    <table id="buttnHolder">
    <tr><td>
		<form action="${pageContext.request.contextPath}/RegServlet" method="post"> 
		<input type="hidden" name="pagename" value="logout"/>
		<input type = "hidden" value="<%out.print(user);%>" name="Username"/>
		<input type = "hidden" value="<%out.print(ipAddress);%>" name="Ip"/>
		<input type="submit" id="logout" value="Log Out" class="blueButton"/>
		</form></td>
		
		<td>
		<button id="editBasicInfo"  class="blueButton">Edit Info</button></td>
	</table>
		</div>
	
	
	
	<!---
	Div Para cambiar la informacion personal Basica
	-->
	
	 <div id="DS1leftEdit" class="hide">
    	<h2> Edit Info</h2>
    	<form action="${pageContext.request.contextPath}/RegServlet" method="post">
    		<input type="hidden" name="pagename" value="updateUserBasicInfo"/>
    		<table>
    			<tr>
    			<td>Username</td>
    			<td><% out.print(user); %></td>
			</tr>
			<tr>
			<td></td>
			<td> <input type="hidden" name="userName" value="<%out.print(viewUser.getUserName());%>"/>
   		 </td>
    	</tr>
		<tr>
		<td> Name</td>
		<td> <input type="text" name="userNewFullName" value="<%out.print(viewUser.getUserFullName()); %>"/>
   		 </td>
    	</tr>
    	<tr><td> Email</td>
    	<td> <input type="text" name="userNewEmail" value="<% out.print(viewUser.getUserEmail()); %>"/>
    	</td>
    </tr>
    <tr><td> Password</td>
    	<td> <input type="password" name="userNewPass" value="<% out.print(viewUser.getUserPass()); %>"/>
    	</td>
    </tr>
    </table>
    
    <input type="submit" id="SaveChanges" value="Save" class="blueButton" >
    </form>
    <button id="cancelbttn" class="blueButton" >Cancel</button>
    

	</div>
	
	
	 <a href="../picture/upload.jsp?type=user&val=<%out.print(user);%>" ><button id="addPic"  class="blueButton">Change Pic</button></a>
	<!---
	Div de la informacin de contacto
	-->	
	
	
  </div>
  <div id="DS1right">
  <h1>Contact Info</h1>
    <table>
  <tr>
  <td>Address:</td>
  <td><%out.print(viewUser.getAddress()); %></td>
  </tr>
  <tr>
  <td>City:</td>
  <td><%out.print(viewUser.getCity()); %></td>
  </tr>
  <tr>
  <td>Province:</td>
  <td><%out.print(viewUser.getProvince()); %></td>
  </tr>
  <tr>
  <td>Phone:</td>
  <td><%out.print(viewUser.getPhone()); %></td>
  </tr>
  
  </table>

  </div>


</div>
<div class = "blueBg" id="adoptions">
  <div class = "containerWidth">
    <h1>Your Pets</h1>
    <div class="darkLeftHalfBox">
   	<h2>On adoption</h2>
   	<h3>Let's find a home for your pet</h3>
   	<button id="cancelbttn" class="whiteButton" >Add Pet</button>
    </div>
    <div class="darkRightHalfBox">
   	<h2>Adopted</h2>
   		<h3>Let's find a pet, check the</h3>
   	<button id="cancelbttn" class="whiteButton" >Classifieds</button> 
    Or 
   	<button id="cancelbttn" class="whiteButton" >Do test</button>
    </div>

  </div>
</div>
<div class = "whiteBg" id="messages">
  <div class = "containerWidth">
    <h1>Thoughts about you</h1>
    
<%
viewUser.addComment("Este es un primer comentario", "alejaro34", "test","12/10/2016");
viewUser.addComment("Este es un Segundo comentario", "amalia3", "test", "12/10/2016");
System.out.println(viewUser.getCommentsNumber());
for (int i=0; i < viewUser.getCommentsNumber(); i++){
	Comment thiscomment = new Comment();
	thiscomment  = viewUser.getComment(i);
	out.print("<div class='profileComment'>");
	out.print("<h3><a href='../user/viewuser.jsp?id="+thiscomment.getOrigin()+"'>"+thiscomment.getOrigin()+"</a> "+thiscomment.getDate()+"</h3>");
	out.print("<p>"+thiscomment.getContent()+"</p><br>");
	out.print("<p>Score Obtained: "+thiscomment.getScore()+"</p><br>");
	out.print("</div>");
}
	


%>


  </div>
</div>
</body>
</html>