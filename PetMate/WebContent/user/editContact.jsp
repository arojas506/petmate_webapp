<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="index.css"/>
 <script type = "text/javascript" src="javascript/jquery-1.7.1.js"> </script>
  <script type = "text/javascript" src="javascript/hide.js"> </script>
  
  <%@page import="Application.*"%>
   <%@page import="bean.DbManager"%>
   <%@page import="java.util.*"%>

<%
  ArrayList<String> animals = DbManager.getList("animals");
  ArrayList<String> provinces = DbManager.getList("provinces");
  
  %>
  <script type = "text/javascript" >
    $(document).ready(function()
		  {
		      function updateFields()
		      {
		    	  $("#petSubType").attr("value","");
		          var petType = $("#petType").attr("value");
		          <%
		          for (int i = 0; i < animals.size(); i++){
		        	  out.println(" if (petType == \""+animals.get(i)+"\")");
		        	  out.println("{$(\"#petSubType\").attr(\"list\",\""+animals.get(i)+"var\")};");
		        	  
		          }
		          %>
		          
		      };
		      $("#petType").bind("change", updateFields);
		  });
  
  </script>
  
  <script type = "text/javascript" >
    $(document).ready(function()
		  {
		      function updateCities()
		      {
		    	  $("#vetCity").attr("value","");
		          var vetProvince = $("#vetProvince").attr("value");
		          <%
		          for (int i = 0; i < provinces.size(); i++){
		        	  out.println(" if (vetProvince == \""+provinces.get(i)+"\")");
		        	  out.println("{$(\"#vetCity\").attr(\"list\",\""+provinces.get(i)+"var\")};");
		        	  
		          }
		          %>
		          
		      };
		      $("#vetProvince").bind("change", updateCities);
		  });
  
  </script>
   <script type = "text/javascript" >
    $(document).ready(function()
		  {
		      function updateCities()
		      {
		    	  $("#petCity").attr("value","");
		          var petProvince = $("#petProvince").attr("value");
		          <%
		          for (int i = 0; i < provinces.size(); i++){
		        	  out.println(" if (petProvince == \""+provinces.get(i)+"\")");
		        	  out.println("{$(\"#petCity\").attr(\"list\",\""+provinces.get(i)+"var\")};");
		        	  
		          }
		          %>
		          
		      };
		      $("#petProvince").bind("change", updateCities);
		  });
  
  </script>
 <datalist id="animalTypes">
 <%

 
 ArrayList<String> cities =  new ArrayList<String> ();
 for (int i =0; i<provinces.size();i++){
	 out.print("<datalist id='"+provinces.get(i)+"var'>");
	 cities  = DbManager.getCitiesList(provinces.get(i));
	 for (int c =0; c< cities.size();c++){
		 out.print("<option value='"+cities.get(c)+"'>");
	 }
	 out.print(" </datalist>");
 }
 
 
 
 %>
  
 <datalist id="provinces">
<%
  for (int i =0; i<provinces.size();i++){
		 out.print("<option value='"+provinces.get(i)+"'>");
	 }
  %>
  </datalist>


















  <title>PetMate Dashboard</title>
  <%@page import="Application.*"%>
   <%@page import="bean.DbManager"%>
   
   <% 	String ipAddress = request.getHeader("X-FORWARDED-FOR");
   		AppUser viewUser = new AppUser();
	   	if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
	   	}
	   	String user = AppSession.getInstance().getUsernameByIp(ipAddress.toString());

	   	if (user.equals("")){
			 out.print("<meta http-equiv='refresh' content='0; url=../register/RegisterUser.jsp' />");
	}
		
		viewUser = DbManager.getUserProfile(user);
	%>
</head>
<body >


<div id="menuContainer">
    <div id="menuContent">
      <table id="menutable">
        <tr>
          <td>
            <a href="../" id="petmet logo" ><img src="images/smalLogo.png" alt="" id="ptmetlogo"/></a>
          </td>
          <td>
            <ul class="nav">
                    <a href="../index.jsp" id="link-start" class="active"><li>Home</li></a>
                    <a href="../adopt/search.jsp" id="link-process" ><li>Adopt</li></a>
                    <a href="../register/RegisterPet.jsp" id="link-portfolio" ><li>Rescue</li></a>
                    <a href="../stats/" id="link-company"><li>Stats</li></a>
                  </ul>
                </td>
              <td class="menuLoginInfo">
               Hello, <%
              
                if (!user.equals("")){
                	String linkDashboradHTMLcodeA = "<a href='../user/dashboard.jsp' >";
                	String linkDashboradHTMLcodeB = "</a>";
                	out.print(linkDashboradHTMLcodeA);
                	out.print(user);
                	out.print(linkDashboradHTMLcodeB);	
                }
                else{
                	String linkDashboradHTMLcodeA = "<a href='../login/index.jsp' >please login</a>";
                	out.print(linkDashboradHTMLcodeA);
                	
                }                
                %> 
              </td>
            </tr>


            </table>

    </div>

  </div>
  

 

  <div id="mainContent">

<!--Titulo -->
<div id="DashboardSection1" class="containerWidth">
    <h1>Edit your contact Info</h1>
<!--Titulo -->
  <form action="${pageContext.request.contextPath}/RegServlet" method="post">
  <input type="hidden" name="pagename" value="editContactInfo"/>
 	<input type="hidden" name="userID" value="<%out.print(user);%>"required>
   <table>
  <tr>
  <td>Address:</td>
  <td><input type="text" name="newAddress" value="" required></td>
  </tr>
 
  <tr>
  <td>Province:</td>
  <td><input  class="dropDown" name="newProvince" id="newProvince" list="provinces" required></td>
  </tr>
  
   <tr>
  <td>City:</td>
  <td><input  class="dropDown" name="petCity" id="petCity" list="" required></td>
  </tr>
  
  
  <tr>
  <td>Phone:</td>
  <td><input type="text" name="newAddress" value="" required></td>
  </tr>
  
  </table>
  <input type="submit" class="blueButton" name="sendTo" value="Save"/>
</form>
  </div>


</div>

</body>
</html>