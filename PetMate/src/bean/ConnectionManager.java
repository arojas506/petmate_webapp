package bean;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	
	private static ConnectionManager instance = null;
	
	private final String USERNAME = 	"ge";
	private final String PASSWORD = 	"ge";
	private final String CONN_STRING = "jdbc:oracle:thin:@localhost:1521:BD1PRJCT";

	private Connection conn = null;

	
	private ConnectionManager(){
	}
	
	public static ConnectionManager getInstance(){
		if(instance == null){
			instance = new ConnectionManager();
		}
		return instance;
	}
	private boolean openConnection(){
		try {
			conn = DriverManager.getConnection(CONN_STRING, USERNAME, PASSWORD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	public Connection getConnection(){
			if(conn == null){
				if(openConnection()){
						return conn;
				}else{
					return null;
				}
			}
			return conn;
	}
	public void close(){
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		conn = null;
	}
}