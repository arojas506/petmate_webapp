
/*
 * DbManager:
 * Esta clase es la que va contiene todos los queries que son enviados al DBMS 
 * y es que que se encarga de obtener los resultados y retornarlos a la aplicacion para su manipulacion.
 * Estos metodos son llamados en RegServlet.java
 * Cada uno de los metodos debe contener esta linea, que es para registrar el driver de la base de datos en la aplicacion:
 *  
 * 				DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
 * 
 * El query se hace de esta forma: (creo q aqui es donde se llaman las funcione y procedimientos que vimos en clase tambien)
 * 
 * String sql	=	"INSERT INTO appUser (userName, userFullName, userEmail, UserPass) VALUES(?,?,?,?)";
 *
 *	Las lineas que se ocupan son:
 *		
 *
 *
 *		PreparedStatement pstmt = null; 
 *		Connection conn = ConnectionManager.getInstance().getConnection();
 *		conn.setAutoCommit(true);
 *		pstmt = conn.prepareStatement(sql);
 *
 * Despues de esto, lo que hacemos es hacer un update del query (como el query tiene "?", estos deben ser reemplazados por parametros que son pasados a cada funcion de dbManager)
 * 
 *		pstmt.setString(1, string1 );
		pstmt.setString(2, ustring2 );
		pstmt.setString(3, string3);
		.
		.
		.
 * Ojo que empieza en uno "pstmt.setString(1, string1 )";. si es un int lo que ocupamos mandarle al query esta el metodo pstmt.setInt( int)
 * luego 
 * 
 * pstmt.executeUpdate(); es para ejectar el query.
 * 
 * entonces el metodo de arriba nos va a retornar informacion depenediendo de lo que estamos haciendo.
 * con el ciclo rs.next()podemos obtener cada celda de cada tupla de infromacion retornada. esta va en orden:
 * sabiendo como es la informacion que vamos a obtenr. (numero de columnas, tipos de dtos. etc) podemos gardar la informacion en alun estructura y retornar la estructura para manjarla mas adelante.
 * 
 * 
 * ResultSet rs	=	pstmt.executeQuery();
		while(rs.next()){
			fn = rs.getString(1);
		}
 * 
 * ConnectionManager.getInstance().close();	es para cerrar la conexion. siempre se debe hacer al final del procedimientos.
 *	
 * 
 * 
 * 
 * 
 */
package bean;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

import Application.AppUser;
import Application.AppPet;
import Application.AdoptionQuestion;
import Application.Comment;
import Application.Contact;
import Application.Illness;
import Application.Med;

public class DbManager {
	
	/*
	 * 
	 * Insersion de usuarios
	 */
	
	
	public static void Insert(AppUser user) throws ClassNotFoundException, SQLException{
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		conn.setAutoCommit(true);
		CallableStatement cstmt = conn.prepareCall ("{Call INSERT_ACCOUNT (?, ?, ?, ?)}");
		cstmt.setString(1, user.getUserName());
		cstmt.setString(2, user.getUserPass());
		cstmt.setString(3, user.getUserEmail());
		cstmt.setString(4, user.getUserFullName());
		cstmt.execute ();
		cstmt.close();
		ConnectionManager.getInstance().close();	
		
	}
	/*
	 * 
	 * Verificar email
	 */
	
	public static int emailExists(String pEmail) throws ClassNotFoundException, SQLException {
		int value;
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{ ? = call Exists_USER_EMAIL (? )}");
		cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.INTEGER);
		cstmt.setString(2, pEmail);	
		cstmt.executeQuery();
		value =  cstmt.getInt(1);
		cstmt.close();
		ConnectionManager.getInstance().close();
		return value;
		
		
	}
	/*
	 * 
	 * verificar usuario
	 */
	
	public static int usernameExists(String pUsername) throws ClassNotFoundException, SQLException {
		int value;
		
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{ ? = call Exists_USER_ACCOUNT (? )}");
		cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.INTEGER);
		cstmt.setString(2, pUsername);
		cstmt.executeQuery();
		value =  cstmt.getInt(1);
		cstmt.close();
		ConnectionManager.getInstance().close();
		return value;
		
		
	}
	
	/*
	 * combinacion usuario + contrase;a
	 */
	
	public static int CheckUser(AppUser user) throws ClassNotFoundException, SQLException{
		int value;
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{ ? = call valid_login_Credentials (?, ?)}");
		cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.INTEGER);
		cstmt.setString(2,user.getUserName());
		cstmt.setString(3,user.getUserPass());
		cstmt.executeQuery();
		value =  cstmt.getInt(1);
		cstmt.close();
		ConnectionManager.getInstance().close();
		return value;
		
	}
	
	/*
	 * 
	 * Update del usuario 
	 */
	

		public static void updateAppUser (String identifier, String newFullName, String newEmail, String newPass) throws ClassNotFoundException, SQLException{
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{Call update_basic_user_info (?, ?, ?, ?)}");
		cstmt.setString(1, identifier);
		cstmt.setString(2, newPass);
		cstmt.setString(3, newEmail);
		cstmt.setString(4, newFullName);
		cstmt.execute ();
		cstmt.close();
		ConnectionManager.getInstance().close();
	}
		
		/*
		 * 
		 *si es admin 
		 */
		
	public static boolean isAdmin(String pUserName) throws ClassNotFoundException, SQLException{
		boolean value =  false;
		int intValue;
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{ ? = call isADMIN (?)}");
		cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.INTEGER);
		cstmt.setString(2,pUserName);
		cstmt.executeQuery();
		intValue =  cstmt.getInt(1);
		cstmt.close();
		ConnectionManager.getInstance().close();
		if (intValue==1) value =true;
		return value;
	}
	
	public static void addAnimal(String pNewAnimal) throws ClassNotFoundException, SQLException{
		/*
		 * 
		 * Insersion de mascotas
		 */
		
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		conn.setAutoCommit(true);
		CallableStatement cstmt = conn.prepareCall ("{Call Insert_PET_TYPE (?)}");
		cstmt.setString(1, pNewAnimal);
		cstmt.execute ();
		cstmt.close();
		ConnectionManager.getInstance().close();
	}
	public static void deleteAnimal(String pAnimal) throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void addBreed (String animalName, String breedName )throws ClassNotFoundException, SQLException{
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		conn.setAutoCommit(true);
		CallableStatement cstmt = conn.prepareCall ("{Call Insert_VARIATION (?)}");
		//cstmt.setString(1, pNewAnimal);
		cstmt.execute ();
		cstmt.close();
		ConnectionManager.getInstance().close();
	}

	public static void deleteBreed (String breedID )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void addPetAtrColor (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}
	public static void addColor (String valueA )throws ClassNotFoundException, SQLException{
	/**
	 * Complete this function
	 * delete animal into db
	 * 
	 */
}

	public static void deleteColor (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void addEnergy (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void deleteEnergy (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void addTraining (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void deleteTraining (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void addIllness (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void deleteIllness (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void addTreatment (String pIllnes, String pTreatment )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void deleteTreatment (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void addDrug (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void deleteDrug (String valueA )throws ClassNotFoundException, SQLException{
		/**
		 * Complete this function
		 * delete animal into db
		 * 
		 */
	}

	public static void  addToAdoptionTest(String question )throws ClassNotFoundException, SQLException{
	/**
	 * Complete this function
	 * delete animal into db
	 * 
	 */
}
	public static void  deleteInAdoptionTest(String question )throws ClassNotFoundException, SQLException{
	/**
	 * Complete this function
	 * delete animal into db
	 * 
	 */
}
	public static void  addAnswerToQuestionAdoptionTest (String pQuestionID, String pAnswer )throws ClassNotFoundException, SQLException{
	/**
	 * Complete this function
	 * delete animal into db
	 * 
	 */
}
	public static void  deleteAnswer (String answer )throws ClassNotFoundException, SQLException{
	/**
	 * Complete this function
	 * delete animal into db
	 * 
	 */
}
	public static void addQuestionToTestFInder(String question, String Attribute ) throws ClassNotFoundException, SQLException{
	/**
	 * Complete this function
	 * delete animal into db
	 * 
	 */
}
	public static void deleteQuestionToTestFInder(String questionID)  throws ClassNotFoundException, SQLException{
	/**
	 * Complete this function
	 * delete animal into db
	 * 
	 */

}
	
	/*
	 * 
	 * 
	 *Obtencion de perfiles
	 * 
	 */
	
	public static AppUser getUserProfile (String userName) throws ClassNotFoundException, SQLException{
		String value = "";
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{ ? = call getUserInfo (? )}");
		cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.VARCHAR);
		cstmt.setString(2,userName);
		cstmt.executeQuery();
		value =  cstmt.getString(1);
		System.out.println(value);
		cstmt.close();
		String[] userInfo = value.split(",");
		ConnectionManager.getInstance().close();
		AppUser user = new AppUser();
		if(!userName.equals("")){
			user.setUserPass(userInfo[2]);
			user.setUserId(Integer.parseInt(userInfo[0]));
			user.setUserFullName(userInfo[5]);
			user.setUserName(userInfo[1]);
			user.setUserEmail(userInfo[3]);
			user.setProvince(userInfo[8]);
			user.setCity(userInfo[7]);
			user.setPhone(userInfo[10]);
			user.setAddress(userInfo[6]);
		}
		return user;
	}
	/*
	 * 
	 * Reportar usuarios
	 */
	
	public static void  fireReport(String pTo, String pfrom,String pReason ,String pAdditional)throws ClassNotFoundException, SQLException{
		System.out.println("[Report request] to:"+pTo+" from: "+pfrom+" Reason: "+pReason+" notes: "+pAdditional);
		
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{call REPORT_ADOPTANT (? )}");
		cstmt.setString(1,pTo);
		cstmt.executeQuery();
		cstmt.close();
	}
	/*
	 * 
	 * Comprobaciond e ralcion de usarios
	 */
	
	public static boolean hasRelation(String userA, String userB)throws ClassNotFoundException, SQLException{
		boolean value = true;
		
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{ ? = call areRelated (?, ?)}");
		cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.BOOLEAN);
		cstmt.setString(2,userA);
		cstmt.setString(3,userB);
		cstmt.executeQuery();
		value =  cstmt.getBoolean(1);
		return value;
	}
	/*
	 * 
	 * Insersion de Coemtarios
	 */
	
	public static void addComment(String pFrom, String pTo, String pContent,  int pScore , int petId)throws ClassNotFoundException, SQLException{
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{call Insert_Evaluation (?,?,?,?,?)}");
		cstmt.setInt(1,petId);
		cstmt.setString(2,pFrom);
		cstmt.setString(3,pTo);
		cstmt.setString(4,pContent);
		cstmt.setInt(5,pScore);
		cstmt.executeQuery();
		cstmt.close();
	}
	public static boolean isPet(String pUser){
		boolean value = true;
		/*
		 * 
		 * 
		 * 
		 */
		return value;
	}
	
	public static ArrayList<AppPet> homePagePets( int months)throws ClassNotFoundException, SQLException{
		ArrayList<AppPet> resultList = new ArrayList<AppPet> ();
		String query = "SELECT * FROM NONADOPTED_PET WHERE STATUS_DATE <= add_months(sysdate, -? )"; 
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(query);
		pstmt.setInt(1,months);
		ResultSet rset = pstmt.executeQuery();
		
		while (rset.next()){
			AppPet newpet = new AppPet();
			newpet.setName(rset.getString(1));
			newpet.setType(rset.getString(2));
			newpet.setBreed(rset.getString(3));
			newpet.setCondition(rset.getString(4));
			newpet.setSize(rset.getString(5));
			newpet.setColorA(rset.getString(6));
			newpet.setId(rset.getInt(9));
			newpet.setOwner(rset.getString(7));
			resultList.add(newpet);
		}
		pstmt.close();
		return resultList;
		
	}
	public static ArrayList<AppPet> getMatchingPets (String matchingName, String matchingType,   String mathchingbreed, String matchingCondition, String matchingSize,  String matchingColor )throws ClassNotFoundException, SQLException{
		ArrayList<AppPet> resultList = new ArrayList<AppPet> ();
		String query = "select * from search_pet where"; 
		if (matchingName == null) matchingName = "";
			
		if (matchingType == null)	matchingType = "";
			if (mathchingbreed == null) mathchingbreed = "";
				if (matchingCondition == null) matchingCondition = "";
					if (matchingSize == null) matchingSize = "";
						if (matchingColor == null) matchingColor = "";

			
		if ( !matchingName.equals("")) query= query+" pet_name = '"+matchingName+"' and";
		if (!matchingType.equals("")) query= query+" type_name = '"+matchingType+"' and";
		if ( !mathchingbreed.equals("")) query= query+" variation_name = '"+matchingType+"' and";
		if ( !matchingCondition.equals("")) query= query+"  grade= '"+matchingCondition+"' and";
		if ( !matchingSize.equals("")) query= query+" psize = '"+matchingSize+"' and";
		if (!matchingColor.equals("")) query= query+" cname = '"+matchingColor+"' and";
		
		
		if (query.equals("select * from search_pet where")) query = "select * from search_pet";
		else{
			query = query.substring(0, query.length()-3);
		}
		System.out.println(query);
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(query);
		ResultSet rset = pstmt.executeQuery();
		
		while (rset.next()){
			AppPet newpet = new AppPet();
			newpet.setName(rset.getString(1));
			newpet.setType(rset.getString(2));
			newpet.setBreed(rset.getString(3));
			newpet.setCondition(rset.getString(4));
			newpet.setSize(rset.getString(5));
			newpet.setColorA(rset.getString(6));
			newpet.setId(rset.getInt(8));
			newpet.setOwner(rset.getString(7));
			resultList.add(newpet);
		}
		pstmt.close();
		return resultList;
		
	}
	public static boolean isPetsFirstOwner (String pUser, String pPetId){
		boolean value = false;
		/*
		 * 
		 * 
		 */
		return value;
		
	}
	public static boolean isPetsCurrentOwner (String pUser, String pPetId){
		boolean value = false;
		/*
		 * 
		 * 
		 */
		return value;
		
	}
	public static AppPet getPetProfile(int pPetId)throws ClassNotFoundException, SQLException{
		String value = "";
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{ ? = call get_pet_info (? )}");
		cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.VARCHAR);
		cstmt.setInt(2,pPetId);
		cstmt.executeQuery();
		value =  cstmt.getString(1);
		System.out.println(value);
		cstmt.close();
		String[] petInfo = value.split(",");
		ConnectionManager.getInstance().close();
		AppPet pet = new AppPet();
		if (!value.equals("NO PET INFO FOUND")){
			pet.setName(petInfo[1]);
			pet.setAbandonDesc(petInfo[3]);
			pet.setColorB(petInfo[8]);
			pet.setColorA(petInfo[7]);
			pet.setBreed(petInfo[5]);
			pet.setEaseOfTraining(petInfo[11]);
			pet.setEnergy(petInfo[9]);
			pet.setType(petInfo[4]);
			pet.setOwner(petInfo[13]);
			pet.setGender(petInfo[6]);
			pet.setSize(petInfo[10]);
			pet.setCondition(petInfo[26]);
			pet.setSpaceRequired(petInfo[12]);
			pet.setAdopted(petInfo[2]);
			pet.addVet(petInfo[20], petInfo[24], petInfo[25], petInfo[21], petInfo[22], petInfo[23]);
			pet.addContact(petInfo[14], petInfo[18], petInfo[19], petInfo[15], petInfo[16], petInfo[17]);
	
		}
		
		return pet;
	}
	
	public static ArrayList<AdoptionQuestion> getAdoptionQuestions()throws ClassNotFoundException, SQLException{
		ArrayList<AdoptionQuestion> questionList = new ArrayList<AdoptionQuestion>();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM VIEW_AF_QUESTION");
		ResultSet rset = pstmt.executeQuery();

		while (rset.next()){
				String question = rset.getString(2);
				int i = rset.getInt(1);
				PreparedStatement pstmt2 = conn.prepareStatement("SELECT * FROM AF_QUESTION_OPTION where AF_QUESTION_ID = ?");
				pstmt2.setInt(1, i);
				ResultSet rset2 = pstmt2.executeQuery();
				AdoptionQuestion question1 = new AdoptionQuestion();
				question1.setQuestion(question);
				question1.setId(i);
				while (rset2.next()){
					question1.addAnswer(rset2.getInt(1),rset2.getString(2), rset2.getInt(3));
					
				}
				questionList.add(question1);
		}

		return questionList;
		
	}
	public static ArrayList<String> getAdoptionQuestionsIDS()throws ClassNotFoundException, SQLException{
		ArrayList<String> questionList = new ArrayList<String>();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from AF_QUESTION ");
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			questionList.add(rset.getString(1));
		}
		pstmt.close();
		return questionList;
	}
	public static void setUserProfilePicture(String pUser, String pURL)throws ClassNotFoundException, SQLException{
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		conn.setAutoCommit(true);
		CallableStatement cstmt = conn.prepareCall ("{Call Insert_USER_PIC (?, ?)}");
		cstmt.setString(1, pUser);
		cstmt.setString(2, pURL);
		cstmt.execute ();
		cstmt.close();
		ConnectionManager.getInstance().close();	
		
	}
	public static String getUserProfilePicture(String user) throws ClassNotFoundException, SQLException{
		String url;
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		CallableStatement cstmt = conn.prepareCall ("{ ? = call get_User_profile_picture (?)}");
		cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.VARCHAR);
		cstmt.setString(2,user);
		cstmt.executeQuery();
		url =  cstmt.getString(1);
		cstmt.close();
		ConnectionManager.getInstance().close();
		return url;
		
	}
	
	public static String stat1A()throws ClassNotFoundException, SQLException{

		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from STATISTICS_1A ");
		ResultSet rset = pstmt.executeQuery();
		String value = "0" ;
		while (rset.next()){
				value = rset.getString(1);

		}
		return value;
	}
	public static String stat1B()throws ClassNotFoundException, SQLException{

		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from STATISTICS_1B ");
		ResultSet rset = pstmt.executeQuery();
		String value = "0" ;
		while (rset.next()){
				value = rset.getString(1);

		}
		return value;
	}
	public static String stat1C()throws ClassNotFoundException, SQLException{

		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from STATISTICS_1C ");
		ResultSet rset = pstmt.executeQuery();
		String value = "0" ;
		while (rset.next()){
				value = rset.getString(1);

		}
		pstmt.close();
		return value;
	}
	public static ArrayList<String> stat2A()throws ClassNotFoundException, SQLException{
		ArrayList<String> values = new ArrayList<String>();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from STATISTICS_2A");
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			values.add(rset.getString(1));
			values.add(rset.getString(2));

		}
		pstmt.close();
		return values;
	}
	public static String stat2B()throws ClassNotFoundException, SQLException{
		String value ="none";
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from STATISTICS_2B ");
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			value = rset.getString(1);
		}
		pstmt.close();
		return value;
	}
	public static ArrayList<String> stat3()throws ClassNotFoundException, SQLException{
		ArrayList<String> values = new ArrayList<String>();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from STATISTICS_3 ");
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			values.add(rset.getString(1));
			values.add(rset.getString(2));

		}
		pstmt.close();
		return values;
	}
	public static ArrayList<String> stat4()throws ClassNotFoundException, SQLException{
		ArrayList<String> values = new ArrayList<String>();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from STATISTICS_4 ");
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			values.add(rset.getString(1));
			values.add(rset.getString(2));
		}
		pstmt.close();
		return values;
	}
	public static ArrayList<String> stat5()throws ClassNotFoundException, SQLException{
		ArrayList<String> values = new ArrayList<String>();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from STATISTICS_5 ");
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			values.add(rset.getString(1));
		}
		pstmt.close();
		return values;
	}
	public static ArrayList<String> getList(String type)throws ClassNotFoundException, SQLException{
		ArrayList<String> values = new ArrayList<String>();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		
		String query =  " Select * from VARIATION where VARIATION.TYPE_ID= (Select TYPE_ID from PET_TYPE where PET_TYPE.TYPE_NAME='"+type+"')";
		if (type.equals("animals")) query = "select * from pet_type";
		if (type.equals("colour")) query = "select * from colour";
		if (type.equals("severity")) query = "select * from severity";
		if (type.equals("illness")) query = "select * from illness";
		if (type.equals("energy")) query = "select * from energy_level";
		if (type.equals("size")) query = "select * from pet_size";
		if (type.equals("train")) query = "select * from train_facility";
		if (type.equals("space")) query = "select * from space_req";
		if (type.equals("illness")) query = "select * from illness";
		if (type.equals("treatment")) query = "select * from treatment";
		if (type.equals("medicament")) query = "select * from medicament";
		if (type.equals("provinces")) query = "select * from province";
		PreparedStatement pstmt = conn.prepareStatement(query);
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			values.add(rset.getString(2));
		}
		pstmt.close();
		return values;
	}
	public static ArrayList<String> getCitiesList(String type)throws ClassNotFoundException, SQLException{
		ArrayList<String> values = new ArrayList<String>();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		String query = "Select * from CITY where PROVINCE_ID= (Select PROVINCE_ID from PROVINCE where PROVINCE.NAME='"+type+"')";
		PreparedStatement pstmt = conn.prepareStatement(query);
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			values.add(rset.getString(2));
		}
		pstmt.close();
		return values;
	}
	public static int insertPet(AppPet pet)throws ClassNotFoundException, SQLException{
		int value;
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		conn.setAutoCommit(true);
		CallableStatement cstmt = conn.prepareCall ("{Call INSERT_VETERINARIAN  (?,?)}");
		cstmt.setString(2,pet.getVet().getEmail());
		cstmt.setString(1,pet.getVet().getName());
		cstmt.execute ();

		cstmt = conn.prepareCall ("{Call Add_PHONE_VETERINARIAN  (?,?,?)}");
		cstmt.setString(1,pet.getVet().getPhone1());
		cstmt.setString(2,pet.getVet().getEmail());
		cstmt.setString(3,"");
		cstmt.execute ();

		cstmt = conn.prepareCall ("{Call Add_ADDRESS_VETERINARIAN  (?,?,?)}");
		cstmt.setString(1,pet.getVet().getCity());
		cstmt.setString(2,pet.getVet().getAddress());
		cstmt.setString(3,pet.getVet().getEmail());
		cstmt.execute ();

		cstmt = conn.prepareCall ("{Call INSERT_CONTACT  (?,?)}");
		cstmt.setString(1,pet.getPetContact().getName());
		cstmt.setString(2,pet.getPetContact().getEmail());
		cstmt.execute ();

		cstmt = conn.prepareCall ("{Call Add_PHONE_CONTACT  (?,?,?)}");
		cstmt.setString(1,pet.getPetContact().getPhone1());
		cstmt.setString(2,pet.getPetContact().getEmail());
		cstmt.setString(3,"");
		cstmt.execute ();

		cstmt = conn.prepareCall ("{Call Add_ADDRESS_CONTACT  (?,?,?)}");
		cstmt.setString(1,pet.getPetContact().getCity());
		cstmt.setString(2,pet.getPetContact().getAddress());
		cstmt.setString(3,pet.getPetContact().getEmail());
		cstmt.execute ();

		cstmt = conn.prepareCall ("{? = call Insert_Pet (?,?,?,?,?,?,?,?,?,?,?,?,?)}");
		cstmt.registerOutParameter(1, oracle.jdbc.OracleTypes.INTEGER);
		cstmt.setString(2,pet.getName());
		cstmt.setString(3,pet.getAbandonDesc());
		cstmt.setString(4,pet.getSize());
		cstmt.setString(5,pet.getGender());
		cstmt.setString(6,pet.getEnergy());
		cstmt.setString(7,pet.getEaseOfTraining());
		cstmt.setString(8,pet.getSpaceRequired());
		cstmt.setString(9,pet.getCondition());
		cstmt.setString(10,pet.getBreed());
		cstmt.setString(11,pet.getOwner());
		cstmt.setString(12,pet.getPetContact().getEmail());
		cstmt.setString(13,pet.getVet().getEmail());
		cstmt.setString(14,pet.getColorA());
		cstmt.executeQuery ();
		value =  cstmt.getInt(1);
	
		ArrayList<Illness> petIllnes = pet.getPetIllnes();
		for (int i = 0; i < petIllnes.size(); i++){
				if ( !petIllnes.get(i).getName().equals( "")){
					System.out.println("Check 0");
					cstmt = conn.prepareCall ("{call  Insert_PETxILLNESS (?,?,?)}");
					cstmt.setInt(1,value);
					cstmt.setString(2,petIllnes.get(i).getName());
					System.out.println(petIllnes.get(i).getDescription());
					cstmt.setString(3, petIllnes.get(i).getDescription() );
					cstmt.executeQuery ();
			}
		}

		cstmt.close();
		ConnectionManager.getInstance().close();
		return value;
	}
	
	public static void setPetPicture(String petId,  String imgUrl)throws ClassNotFoundException, SQLException{
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		conn.setAutoCommit(true);
		CallableStatement cstmt = conn.prepareCall ("{Call Insert_PET_PIC (?, ?)}");
		cstmt.setInt(1, Integer.parseInt(petId));
		cstmt.setString(2, imgUrl);
		cstmt.execute ();
		cstmt.close();
		
		

	}
	
	
	public static ArrayList<String> loadPetImageSet(int petId )throws ClassNotFoundException, SQLException{
		ArrayList<String> imageList = new ArrayList<String> ();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select IMAGE from photo where PET_ID = ? ");
		pstmt.setInt(1, petId);
		ResultSet rset = pstmt.executeQuery();
		String value;
		while (rset.next()){
				value = rset.getString(1);
				imageList.add(value);
		}
		pstmt.close();
		System.out.println("IMAGE LIST SIZE  FOR PETID "+Integer.toString(petId)+" = "+Integer.toString(imageList.size()));
		return imageList;
		
		
		
	}
	
	public static ArrayList<Comment> getComments(String user )throws ClassNotFoundException, SQLException{
		ArrayList<Comment> commentList = new ArrayList<Comment> ();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from EVALUATION where adoptant_id = (select ACCOUNT_ID from account where username = ?) ");
		pstmt.setString(1, user);
		ResultSet rset = pstmt.executeQuery();
		
		while (rset.next()){
			Comment value = new Comment();
			value.setContent(rset.getString(4));
			value.setScore(rset.getInt(5));
			commentList.add(value);
		}
		pstmt.close();
		
		return commentList;
		
		
		
	}
	
	public static String getPetDate(int petId )throws ClassNotFoundException, SQLException{
		String date = new String ();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select status_date from pet_history where pet_id = ? ");
		pstmt.setInt(1, petId);
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			date = rset.getString(1);
		}
		pstmt.close();
		
		return date;
		
		
		
	}
	
	public static ArrayList<String> getPetHistory(int petId )throws ClassNotFoundException, SQLException{
		ArrayList<String> history = new ArrayList<String> ();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select  pet_history.STATUS_DATE, status.TYPE_STATUS from pet_history left join status using (status_id) where pet_id = ?");
		pstmt.setInt(1, petId);
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			history.add(rset.getString(1));
			history.add(rset.getString(2));
		}
		pstmt.close();
		
		return history;
		
		
		
	}
	
	public static boolean isBlacklisted(int userId)throws ClassNotFoundException, SQLException{
		boolean value = false;
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("select * from get_blacklisted where account_id = ? ");
		pstmt.setInt(1, userId);
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			if (rset.getInt(1)==1) value= true;

		}
		pstmt.close();
		return value;
	}
	
	public static ArrayList<String> getIllList1(int petId )throws ClassNotFoundException, SQLException{
		ArrayList<String> list = new ArrayList<String> ();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("SELECT INAME, PDATE  FROM VIEW_ILLNESS where PET_ID= ?");
		pstmt.setInt(1, petId);
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			list.add(rset.getString(1));
			list.add(rset.getString(2));
		}
		pstmt.close();
		
		return list;
	}
	
	//SELECT INAME, PDATE  FROM VIEW_ILLNESS where PET_ID=1;
	
	public static ArrayList<String> getIllList2(int petId )throws ClassNotFoundException, SQLException{
		ArrayList<String> list = new ArrayList<String> ();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("SELECT TNAME FROM VIEW_TREATMENT where PET_ID= ?");
		pstmt.setInt(1, petId);
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			list.add(rset.getString(1));
		}
		pstmt.close();
		
		return list;
	}
	public static ArrayList<String> getIllList3(int petId )throws ClassNotFoundException, SQLException{
		ArrayList<String> list = new ArrayList<String> ();
		DriverManager.registerDriver (new oracle.jdbc.OracleDriver());
		Connection conn = ConnectionManager.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement("SELECT MNAME, QUANTITY FROM VIEW_MEDICAMENT where PET_ID= ?");
		pstmt.setInt(1, petId);
		ResultSet rset = pstmt.executeQuery();
		while (rset.next()){
			list.add(rset.getString(1));
			list.add(rset.getString(2));
		}
		pstmt.close();
		
		return list;
	}
}