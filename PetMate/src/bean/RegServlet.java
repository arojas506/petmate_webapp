/*
 * 

 * 
 * RegServlet.java
 * Autor: Alejandro Rojas, Bernal Gonzalez
 * Descripcion:	Servlet que funciona como interfaz de comunicacion entre los JSP's 
 * 				y las otras clases del proyecto (dbManager, appSession)
 * 
 * 
 * 
 * Regservlet.java se encarga de recibir informacion de la pagina e indetificar que querys debe hacer en la DB
 * y retornar el resultado. Aun no tengo lcaro como funciona el doGet(creo que s para mnadar info a la pagina web)
 * 
 * pero funciona asi:
 * Los forms que se hacen en los .jsp (<form></form>) empiezan similar a esta linea:
 * 
 * 			 <form action="${pageContext.request.contextPath}/RegServlet" method="post">
 * 
 * eso nos va a mandar directamente a este archivo java. luego  la linea:
 * 
 *  	<input type="hidden" name="pagename" value="register"/>
 *  
 *  Nos va a permitir identifcar el tipo de transaccion que debemos hacer. ya sea registrar, logear, cambiar datos en la bae de datos etc:
 *  
 *  
 *  		String hdnParam	=	request.getParameter("pagename");
 *  
 *  		if(hdnParam.equals("register")){
 *  			...
 *  		}
 *  
 *   dentro del form cada input tiene un atribiuto name:
 *   
 *   				 <input type="text" name="regUserName" .../>
 *   
 *   se puede obptener lo que el usuario escribio en ese input antes de enviar el form d esta forma:
 *   		
 *   		String pUser	=	request.getParameter("regUserName");
 *   
 *   si el usuario escribio "user" pUser ahora tiene este valor.
 *   
 *   luego si la funcion es actualizar o crear datos en la DB se hace asi:
 *   dentro de lo try hacemos la operacion con dbManager,
 *   mandamos la informacion que ocupemos para hacer las consultas.
 *   
 *   un vez hecha la operacion, podemos redireccionar al usuario a alguna pagina en especifico asi.
 *   pueden investigar otras funcionalidades de response.
 *   
 *   response.sendRedirect("user/dashboard.jsp");
 *   		
 *   	
 *   			DbManager.Insert(pUser);
 *   
 *   
 *   
 *   		try {
				DbManager.Insert(user);
				response.sendRedirect("user/dashboard.jsp");
				
			} catch (SQLException e) {
				response.sendRedirect("error/username.jsp");
				e.printStackTrace();
			}
			catch( ClassNotFoundException e ){
				e.printStackTrace();
				
			}
 *   
 *  
 *  
 * 
 * 
 */
package bean;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Application.AppPet;
import Application.AppPetSession;
import Application.AppSession;
import Application.AppUser;
import Application.Contact;
import Application.Illness;
import Application.Med;
import Application.SendMail;

//File uploading libs













import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;












import org.apache.tomcat.util.http.fileupload.*;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;
/**
 * Servlet implementation class RegServlet
 */
@WebServlet(name="RegServlet",urlPatterns={"/RegServlet"})
public class RegServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String hdnParam	=	request.getParameter("pagename");
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 * register
		 * 
		 * 
		 * 
		 * 
		 */
		if(hdnParam.equals("register")){
			String fname	=	request.getParameter("regUserName");
			String fpass	=	request.getParameter("regUserPass");
			String ffname	=	request.getParameter("regUserFullName");
			String femail	=	request.getParameter("regUserEmail");
			AppUser user = new AppUser();
			user.setUserName(fname);
			user.setUserPass(fpass);
			user.setUserEmail(femail);
			user.setUserFullName(ffname);
			try {
				if (DbManager.emailExists(femail)== 0){
					if (DbManager.usernameExists(fname)==0){
						DbManager.Insert(user);
						String ipAddress = request.getHeader("X-FORWARDED-FOR");  
						   if (ipAddress == null) { 
							   ipAddress = request.getRemoteAddr();  
						   }
						   AppSession.getInstance().startSession(ipAddress,fname);
						   response.sendRedirect("user/dashboard.jsp");
					}
					else{
					response.sendRedirect("register/RegisterUser.jsp?msg=Username already taken");}
				}
				else{
					response.sendRedirect("register/RegisterUser.jsp?msg=That email is already being used");
				}
			} catch (SQLException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			}
			catch( ClassNotFoundException e ){
				e.printStackTrace();
				
			}
		}
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 * Login
		 * 
		 * 
		 * 
		 * 
		 */
		if (hdnParam.equals("login")){
			String userName	=	request.getParameter("Username");
			String password	=	request.getParameter("Password");
				
				AppUser user	=	new AppUser();
				
				user.setUserName(userName);
				user.setUserPass(password);
				try {
					int checkUser	=	DbManager.CheckUser(user);
						if(checkUser == 1){
							
							String ipAddress = request.getHeader("X-FORWARDED-FOR");  
							   if (ipAddress == null) {  
								   ipAddress = request.getRemoteAddr();  
							   }
							   AppSession.getInstance().startSession(ipAddress,userName);
							   if (DbManager.isAdmin(userName)) response.sendRedirect("admin/adminDashboard.jsp");
							   else response.sendRedirect("user/dashboard.jsp");
						}else{
							response.sendRedirect("login/index.jsp?msg=Wrong Credentials");
						}
				} catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
					response.sendRedirect("error/error.jsp?error="+e.getMessage());
				
			}
			
			
		}
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 * Logout
		 * 
		 * 
		 * 
		 * 
		 */
		if (hdnParam.equals("logout")){
			String pUser	=	request.getParameter("Username");
			String pClientIp	=	request.getParameter("Ip");
			AppSession.getInstance().endSession(pClientIp, pUser);
			response.sendRedirect("index.jsp");
		}
		
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 * Update
		 * 
		 * 
		 * 
		 * 
		 */
		
		if (hdnParam.equals("updateUserBasicInfo")){
			String pUser		=	request.getParameter("userName");
			String pNewEmail	=	request.getParameter("userNewEmail");
			String pNewPass		=	request.getParameter("userNewPass");
			String pNewFName	=	request.getParameter("userNewFullName");
			try {
					DbManager.updateAppUser(pUser,pNewFName, pNewEmail, pNewPass );
					System.out.println("[FN Updated, Email Updated] ");
					response.sendRedirect("user/dashboard.jsp");
				} catch (ClassNotFoundException e) {
						
					e.printStackTrace();
				} catch (SQLException e) {
					String message = e.getMessage();
					if (message.equals("Invalid column index")){
						System.out.println("Error: BUT: [FN Updated, Email Updated] ");
						response.sendRedirect("user/dashboard.jsp");
						
					}
					else{
						response.sendRedirect("error/error.jsp?error="+e.getMessage());
						e.printStackTrace();
					}
					
				}
		}
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 * admin funtionalities
		 * 
		 * 
		 * 
		 * 
		 */
		if (hdnParam.equals("addAnimalName")){
			String pAnimal=request.getParameter("newAnimalName");
			try {
				DbManager.addAnimal(pAnimal );
				System.out.print("Animal Added: ");
				System.out.println(pAnimal);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Animal Addded with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error=Error&adding&animal");
					e.printStackTrace();
				}
				
			}

			
		}

		if (hdnParam.equals("deleteAnimalName")){
			
			String pAnimal=request.getParameter("deleteAnimalName");
			try {
				DbManager.deleteAnimal(pAnimal );
				System.out.print("Animal Deleted: ");
				System.out.println(pAnimal);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Animal Addded with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error=Error&deleting&animal");
					e.printStackTrace();
				}
				
			}
			
			
			
		}
		if (hdnParam.equals("addBreed")){
			String pAnimal=request.getParameter("matchinganimalName");
			String pNewBreed=request.getParameter("animalbreedName");
			try {
				DbManager.addBreed(pAnimal, pNewBreed );
				System.out.print("Breed Added: ");
				System.out.print(pNewBreed);
				System.out.print("--->");
				System.out.println(pAnimal);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("breed Addded with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/process.jsp");
					e.printStackTrace();
				}
				
			}
			
			
			
			
			
		}
		if (hdnParam.equals("deleteBreed")){
			String pBreedID=request.getParameter("animalbreedID");
			try {
				DbManager.deleteBreed( pBreedID);
				System.out.print("Breed Deleted: ");
				System.out.print(pBreedID);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("breed deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error=Error&deleting&breed");
					e.printStackTrace();
				}
				
			}
			
			
			
			
			
		}
		if (hdnParam.equals("addColor")){
			
			String pColor=request.getParameter("newColor");
		
			try {
				DbManager.addColor(pColor);
				System.out.print("Color Added: ");
				System.out.println(pColor);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("breed deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error=Error&adding&color");
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("deleteColor")){
				
				String pColor=request.getParameter("deleteColor");
			
				try {
					DbManager.deleteColor(pColor);
					System.out.print("Color Deleted: ");
					System.out.println(pColor);
					response.sendRedirect("admin/adminDashboard.jsp");
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e ) {
					String message = e.getMessage();
					if (message.equals("Invalid column index")){
						System.out.println("Color deleted with errors");
						response.sendRedirect("user/dashboard.jsp");
					}
					else{
						response.sendRedirect("error/error.jsp?error=Error&deleting&color");
						e.printStackTrace();
					}
					
				}
				
				
			
		}
		if (hdnParam.equals("addEnergy")){
			String pEnergy=request.getParameter("newEnergyLevel");
			
			try {
				DbManager.addEnergy(pEnergy);
				System.out.print("Energy Added: ");
				System.out.println(pEnergy);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error=Error&adding&energy");
					e.printStackTrace();
				}
				
			}
			
			
		}
		if (hdnParam.equals("deleteEnergy")){
			String pEnergy=request.getParameter("deleteEnergyLevel");
			
			try {
				DbManager.deleteEnergy(pEnergy);
				System.out.print("Energy Deleted: ");
				System.out.println(pEnergy);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error=Error&adding&energy");
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("addTraining")){
			String pTraining=request.getParameter("trainingLevel");
			
			try {
				DbManager.addTraining(pTraining);
				System.out.print("Training Added: ");
				System.out.println(pTraining);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/process.jsp");
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("deleteTraining")){
			String pTraining=request.getParameter("deleteTraining");
			
			try {
				DbManager.deleteTraining(pTraining);
				System.out.print("Training Deleted: ");
				System.out.println(pTraining);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/process.jsp");
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("addPetIllNess")){
			String pIllness=request.getParameter("addPetIllnes");
			
			try {
				DbManager.addIllness(pIllness);
				System.out.print("Illness Added: ");
				System.out.println(pIllness);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/process.jsp");
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("deletePetIllNess")){
			String pIllness=request.getParameter("deletePetIllNess");
			
			try {
				DbManager.deleteIllness(pIllness);
				System.out.print("Illness Deleted: ");
				System.out.println(pIllness);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/process.jsp");
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("addTreatment")){
			String pIllnes=request.getParameter("matchIlness");
			String pTreatment=request.getParameter("addTreatmentName");
			
			try {
				DbManager.addTreatment(pIllnes,pTreatment);
				System.out.print("Treatment Aded: ");
				System.out.print(pIllnes);
				System.out.println(pTreatment);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/process.jsp");
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("deleteTreatment")){
			String pTreatment=request.getParameter("treatmentID");
			
			try {
				DbManager.deleteTreatment(pTreatment);
				System.out.print("Treatment Deleted: ");
				System.out.println(pTreatment);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error="+e.getMessage());
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("addDrug")){
			String pDrug=request.getParameter("drugName");
			
			try {
				DbManager.addDrug(pDrug);
				System.out.print("Drug added: ");
				System.out.println(pDrug);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error="+e.getMessage());
					e.printStackTrace();
				}
				
			}
		}
		if (hdnParam.equals("deleteDrugName")){
			String pDrug=request.getParameter("drugName");	
			try {
				DbManager.deleteDrug(pDrug);
				System.out.print("Drug Deleted: ");
				System.out.println(pDrug);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/process.jsp");
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("addQuestionAdoptionTest")){
			String pNewQuestion=request.getParameter("newQuestion");	
			try {
				DbManager.addToAdoptionTest(pNewQuestion);
				System.out.print("Question Added: ");
				System.out.println(pNewQuestion);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/process.jsp");
					e.printStackTrace();
				}
				
			}
			
			
		}
		if (hdnParam.equals("deleteQuestionAdoptionTest")){
			String pQuestionID=request.getParameter("questionID");	
			
			try {
				DbManager.deleteInAdoptionTest(pQuestionID);
				System.out.print("Question Deleted: ");
				System.out.println(pQuestionID);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error="+e.getMessage());
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("addAnswerAdoptionTest")){
			String pQuestionID=request.getParameter("matchQuestionID");	
			String pNewAnswer=request.getParameter("newAnswer");	
			try {
				DbManager.addAnswerToQuestionAdoptionTest(pQuestionID, pNewAnswer);
				System.out.print("Answer Added: ");
				System.out.print(pNewAnswer);
				System.out.print("  to: ");
				System.out.println(pQuestionID);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error="+e.getMessage());
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("deleteAnswerAdoptionTest")){
	
			String pAnswerID=request.getParameter("answerId");	
			try {
				DbManager.deleteAnswer(pAnswerID);
				System.out.print("Answer deleted by ID: ");
				System.out.println(pAnswerID);
				
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error="+e.getMessage());
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("addQuestionFindPet")){
			String newFindQuestion=request.getParameter("newQuestion");	
			String forAtribute=request.getParameter("matchingAtrr");
			try {
				DbManager.addQuestionToTestFInder(newFindQuestion,forAtribute);
				System.out.print("Question Added to pet finder: ");
				System.out.print(newFindQuestion);
				System.out.print("  Matching Attr: ");
				System.out.println(forAtribute);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error="+e.getMessage());
					e.printStackTrace();
				}
				
			}
			
		}
		if (hdnParam.equals("deleteQuestionFindPet")){
			String questionID=request.getParameter("questionID");	
			try {
				DbManager.deleteQuestionToTestFInder(questionID);
				System.out.print("Question Deleted: ");
				System.out.println(questionID);
				response.sendRedirect("admin/adminDashboard.jsp");
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e ) {
				String message = e.getMessage();
				if (message.equals("Invalid column index")){
					System.out.println("Color deleted with errors");
					response.sendRedirect("user/dashboard.jsp");
				}
				else{
					response.sendRedirect("error/error.jsp?error="+e.getMessage());
					e.printStackTrace();
				}
				
			}
			
		}
		
		if (hdnParam.equals("flagUser")){
			String reportedTo=request.getParameter("target");
			String reportedFrom=request.getParameter("from");
			if (reportedTo.equals(reportedFrom)){
				response.sendRedirect("error/error.jsp");
			}
			String forReason=request.getParameter("reason");
			String additionalInfo=request.getParameter("message");
			try {
				DbManager.fireReport(reportedTo,reportedFrom,forReason,additionalInfo);
				response.sendRedirect("success/?msg=The report was sent");
				
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 * Send email
		 * 
		 * 
		 * 
		 * 
		 */
		if (hdnParam.equals("sendEmail")){
			String pTo = request.getParameter("target");
			String pFrom = request.getParameter("from");
			String pSubject = request.getParameter("subject");
			String pContent = request.getParameter("message");
			SendMail.SendEmail(pTo, pFrom, pSubject, pContent);
			response.sendRedirect("success/?msg=The email was sent");
			
		}
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 * add comment
		 * 
		 * 
		 * 
		 * 
		 */
		
		if (hdnParam.equals("addComment")){
			String petId = request.getParameter("petId");
			String pTo = request.getParameter("target");
			String pFrom = request.getParameter("writer");
			String pScore = request.getParameter("score");
			String pContent = request.getParameter("commentValue");
			int pScoreInt = Integer.parseInt(pScore);
			int petIdInt =Integer.parseInt( petId);
			try {
				DbManager.addComment(pFrom, pTo, pContent ,pScoreInt,petIdInt );
				response.sendRedirect("user/viewuser.jsp?id="+pTo);
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			}
			
			
		}
		/*
		 * 
		 * 
		 * 
		 * 
		 * 
		 * get adoption
		 * 
		 * 
		 * 
		 * 
		 */
		if (hdnParam.equals("adoptionRequest")){
			ArrayList<String> questionIds = new ArrayList<String>();
			try {
				questionIds= DbManager.getAdoptionQuestionsIDS();
			} catch (ClassNotFoundException | SQLException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			}
			String user= request.getParameter("requser"); 
			String petId= request.getParameter("reqpetId");
			
			for(int i= 0; i < questionIds.size(); i++){
				String pQAnswerID= request.getParameter(questionIds.get(i)); 
				System.out.println("Qid="+questionIds.get(i)+" //AnswerId="+pQAnswerID+" //User="+user+" //Pet="+petId);
			
			}
			response.sendRedirect("adopt/viewPet.jsp?id="+petId);
			
		}
		if (hdnParam.equals("registerPet")){
			String   ipAddress   =request.getParameter("ipAddress");
			System.out.println("Servlet Called");
			String   owner   =request.getParameter("owner");
			String    petType  =request.getParameter("petType");
			String   petSubType   =request.getParameter("petSubType");
			String    petName  =request.getParameter("petName");
			String    petGender  =request.getParameter("petGender");
			String     petColor1 =request.getParameter("petColor1");
			String   petColor2   =request.getParameter("petColor2");
			String   petEnergy   =request.getParameter("petEnergy");
			String    petSize  =request.getParameter("petSize");
			String  petTrain    =request.getParameter("petTrain");
			String   petSpace   =request.getParameter("petSpace");
			String    petAbandonDesc  =request.getParameter("petAbandonDesc");
			String  petHealthStatus    =request.getParameter("petHealthStatus");
			
			
			String   petHealthIllnes1   =request.getParameter("petHealthIllnes1");
			
			String    petHealthIllnesDesc1  =request.getParameter("petHealthIllnesDesc1");
			
			if ( petHealthIllnes1 == null){
				petHealthIllnesDesc1 ="";
				petHealthIllnesDesc1 = "";
			}
			
			String    petHealthIllnes2  =request.getParameter("petHealthIllnes2");
			String    petHealthIllnesDesc2  =request.getParameter("petHealthIllnesDesc2");
			
			if ( petHealthIllnes2 == null){
				petHealthIllnes2 ="";
				petHealthIllnesDesc2 = "";
			}
			
			
			String   petHealthIllnes3   =request.getParameter("petHealthIllnes3");

			String     petHealthIllnesDesc3 =request.getParameter("petHealthIllnesDesc3");
			
			if ( petHealthIllnes3 == null){
				petHealthIllnes2 ="";
				petHealthIllnesDesc3 = "";
			}
			

			String    vetName  =request.getParameter("vetName");
			String    vetEmail  =request.getParameter("vetEmail");
			String  vetPhone    =request.getParameter("vetPhone");
			String  vetProvince    =request.getParameter("vetProvince");
			String   vetCity   =request.getParameter("vetCity");
		    
			String    vetAddress  =request.getParameter("vetAddress");
			String    petAddress  =request.getParameter("petAddress");
			String   petCity   =request.getParameter("petCity");
			String  petProvince    =request.getParameter("petProvince");
			String petPhone     =request.getParameter("petPhone");
			String petEmail     =request.getParameter("petEmail");
			String   peContactName   =request.getParameter("peContactName");
			AppPet pet = new AppPet();
			pet.setColorB(petColor2);
			pet.setColorA(petColor1);
			pet.setBreed(petSubType);
			pet.setName(petName);;
			pet.setEaseOfTraining(petTrain);
			pet.setEnergy(petEnergy);
			pet.setType(petType);
			pet.setOwner(owner);
			pet.setGender(petGender);
			pet.setSize(petSize);
			pet.setCondition(petHealthStatus);
			pet.setSpaceRequired(petSpace);

			pet.setAbandonDesc(petAbandonDesc);
		
			Illness ill1 = new Illness(petHealthIllnesDesc1, petHealthIllnes1);
			Illness ill2 = new Illness(petHealthIllnesDesc2, petHealthIllnes2);
			Illness ill3 = new Illness(petHealthIllnesDesc3, petHealthIllnes3);
			ArrayList<Illness> petilllist = new ArrayList<Illness>();
			petilllist.add(ill1);petilllist.add(ill2);petilllist.add(ill3);
			Contact vetContact = new Contact();
			vetContact.setAddress(vetAddress);
			vetContact.setCity(vetCity);
			vetContact.setEmail(vetEmail);
			vetContact.setName(vetName);
			vetContact.setPhone1(vetPhone);
			vetContact.setState(vetProvince);
			Contact petContact = new Contact();
			petContact.setAddress(petAddress);
			petContact.setCity(petCity);
			petContact.setEmail(petEmail);
			petContact.setName(peContactName);
			petContact.setPhone1(petPhone);
			petContact.setState(petProvince);
			pet.setPetIllnes(petilllist);
			pet.setPetContact(petContact);
			pet.setVet(vetContact);
			try {

				int petId = DbManager.insertPet(pet);
				System.out.println("Servlet Calling DBmanger");
				AppPetSession.getInstance().startSession(ipAddress,Integer.toString(petId));
				response.sendRedirect("picture/uploadPet.jsp");
				
				
				
			} catch (ClassNotFoundException e) {
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
				e.printStackTrace();
			} catch (SQLException e) {
				
				e.printStackTrace();
				response.sendRedirect("error/error.jsp?error="+e.getMessage());
			}
			
		}
		if (hdnParam.equals("addNewPetPic")){
			String   ipAddress   =request.getParameter("ipAddress");
			String   petId   =request.getParameter("petId");
			AppPetSession.getInstance().startSession(ipAddress,petId);
			response.sendRedirect("picture/uploadPet.jsp");
			
		}
		
		
	
	}
}
		


