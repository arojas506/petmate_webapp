package bean;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

 

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;
/**
 * Servlet implementation class Uploader
 */
public class PetUploader extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PetUploader() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		;
		PrintWriter out = response.getWriter();
		   
		  if(!ServletFileUpload.isMultipartContent(request)){
		   out.println("Nothing to upload");
		   return; 
		  }
		  FileItemFactory itemfactory = new DiskFileItemFactory(); 
		  ServletFileUpload upload = new ServletFileUpload(itemfactory);
		  try{
			 List<FileItem>  items = upload.parseRequest(new ServletRequestContext(request));
			 for(FileItem item:items){
		   
		    out.println(item.getSize());
		    if (item.getSize()>40000){
		    	File uploadDir = new File("C:\\Users\\Alejandro\\git\\petMate2\\PetMate\\WebContent\\images");
		    	File file = File.createTempFile("petmate_img",".png",uploadDir);
			    item.write(file);
			    response.sendRedirect("picture/getPetPic.jsp?id="+file.getName());
			    /*
			     * 
			     * System.out.println(file.getAbsoluteFile());
			     * System.out.println(file.getName());
			     * op
			     */

			  
			    
		    }
		   }
		  }
		  catch(FileUploadException e){
		    
		   out.println("upload fail");
		  }
		  catch(Exception ex){
		   System.out.println(ex.getMessage());
		   out.println("can't save");
		  }
	}

}
