package Application;

public class Illness {
	private int id;
	private String name;
	private String treatment;
	private String description;
	public Illness( ){
	}
	
	public Illness(String pDescription, String pName){
		this.name= pName;
		this.description = pDescription;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTreatment() {
		return treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

}
