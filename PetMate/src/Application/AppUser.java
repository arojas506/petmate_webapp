package Application;

import java.util.ArrayList;

public class AppUser {
	private int userId;
	private String address;
	private String province;
	private String phone;
	private String city;
	private String userName;
	private String userPass;
	private String userFullName;
	private String userEmail;
	private boolean userActive;
	private String userSignInDate;
	private ArrayList<Comment> comments;
	private ArrayList<Adoption> adoptions;
	private ArrayList<Adoption> adopted;
	private boolean hasContactID;
	
	
	public AppUser(){
		this.userActive = true;
		this.adoptions = new ArrayList<Adoption>();
		this.adopted= new ArrayList<Adoption>();
		this.comments= new ArrayList<Comment>();
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPass() {
		return userPass;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	public boolean isUserActive() {
		return userActive;
	}
	public void setUserActive(boolean userActive) {
		this.userActive = userActive;
	}
	public String getUserSignInDate() {
		return userSignInDate;
	}
	public void setUserSignInDate(String userSignInDate) {
		this.userSignInDate = userSignInDate;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public ArrayList<Comment> getComments() {
		return comments;
	}

	public void addComment(String content, String origin, String destiny, String pDate ) {
		Comment newComment =  new Comment();
		newComment.setContent(content);
		newComment.setDate(pDate);
		newComment.setOrigin(origin);
		comments.add(newComment);
	}

	public boolean isHasContactID() {
		return hasContactID;
	}

	public void setHasContactID(boolean hasContactID) {
		this.hasContactID = hasContactID;
	}
	public boolean hasOnAdoption(){
		int size = this.adoptions.size();
		if (size > 0){
			return true;
		}
		else{
			return false;
		}
		
	}
	public boolean hasAdopted(){
		int size = this.adopted.size();
		if (size > 0){
			return true;
		}
		else{
			return false;
		}
		
	}
	public int getCommentsNumber(){
		return comments.size();
	}
	public Comment getComment(int index){
		return comments.get(index);
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}
