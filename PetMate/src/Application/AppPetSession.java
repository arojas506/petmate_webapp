
/*
 * AppSession.java
 * Autor: Alejandro Rojas, Bernal Gonzalez
 * Descripcion: Simula una session con el servidor, usando el usario en la base (previamente autenticado) 
 * 				y la Ip del cliente.
 * 
 * 
 * 
 * 
 * 
 */
package Application;

import java.util.ArrayList;

public class AppPetSession {
	private String clientIP;
	private String petId;
	private static ArrayList<AppPetSession> sessionsList= new ArrayList<AppPetSession>();
	private static AppPetSession instance;
	
	public AppPetSession(){
		clientIP = null;
		petId = null;
	}
	
	public static AppPetSession getInstance(){
		if (instance==null){
			instance= new AppPetSession();
		}
		return instance;
	}
	
	public void startSession(String ipString, String pPetId){
		this.clientIP= ipString;
		this.petId = pPetId;
		System.out.println(ipString+"->"+pPetId);
		sessionsList.add(this);
		
	}
	public void endSession(String ipString, String pPetId){
		for (int i=0; i < sessionsList.size(); i++){
			AppPetSession search = sessionsList.get(i);
			if (search.getClientIP().equals(ipString) && search.getClientUserName().equals(pPetId)){
				sessionsList.remove(i);
			}
		}
		
	}
	public static boolean  isIpLogged(String ipString){
		if (sessionsList.contains(ipString)) return true;
		else return false;
		
	}
	void endSession(String ipString){
		if (isIpLogged(ipString)) sessionsList.remove(ipString);
	}
	
	public String getClientIP() {
		return clientIP;
	}
	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}
	public String getClientUserName() {
		return petId;
	}
	public void setClientUserName(String pPetId) {
		this.petId = pPetId;
	}
	
	public String getUsernameByIp(String clientIP){
		String clientUsername = "";
		for(int i = 0; i < sessionsList.size() ; i++  ){
			AppPetSession search =  sessionsList.get(i);
			if (search.getClientIP().toString().equals(clientIP)){
				clientUsername = search.getClientUserName();
			}
		}
		return clientUsername; 
		
	}
	
	public ArrayList<AppPetSession> getSessionList(){
		return sessionsList;		
	}

	public String getPetId() {
		return petId;
	}

	public void setPetId(String petId) {
		this.petId = petId;
	}

}
