package Application;

public class Adoption {
	private int id;
	private String rescuer;
	private String receiver;
	private String type;
	private String petId;
	private String date;
	public Adoption( ){
	}
	
	public Adoption(String pRescuer, String pAdoptant, String pDate, String pType ){
		this.date= pDate;
		this.receiver = pAdoptant;
		this.rescuer = pRescuer;
		this.type = pType;
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGiver() {
		return rescuer;
	}
	public void setGiver(String giver) {
		this.rescuer = giver;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String pReceiver) {
		this.receiver = pReceiver;
	}
	public String getPetId() {
		return petId;
	}
	public void setPetId(String petId) {
		this.petId = petId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
