package Application;

import java.util.ArrayList;

public class AppPet {
	private int id;
	private String spaceRequired;
	private ArrayList<Adoption> history;
	private ArrayList<Med> petDrugs;
	private ArrayList<Illness> petIllnes;
	private String owner;
	private String receiver;
	private String type;
	private String breed;
	private String date;
	private String energy;
	private String condition;
	private String easeOfTraining;
	private String colorA;
	private String colorB;
	private String gender;
	private String  size;
	private String isAdopted;
	private String imgURL1;
	private String imgURL2;
	private String name;
	private Contact vet;
	private Contact petContact;
	private String abandonDesc;
	
	public AppPet(){
		this.imgURL1 = null;
		this.imgURL2 = null;
		this.colorA = null;
		this.setColorB(null);
		this.condition = null;
		this.owner = null;
		this.colorA= null;
		this.size =null;
		this.name = null;
		this.type = null;
		this.breed = null;
		this.petDrugs = new ArrayList<Med>();
		this.history = new ArrayList<Adoption>();
		this.petIllnes= new ArrayList<Illness>();
	}
	
	public AppPet(int id, String pName, String pOwner, String pCond, String pColorA,String pColorB, String pSize, String pType, String pBreed ){
		this.id = id;
		this.setColorB(pColorB);
		this.condition = pCond;
		this.owner = pOwner;
		this.colorA= pColorA;
		this.size =pSize;
		this.name = pName;
		this.type = pType;
		this.breed = pBreed;
		this.petDrugs = new ArrayList<Med>();
		this.history = new ArrayList<Adoption>();
		this.petIllnes= new ArrayList<Illness>();	
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGiver() {
		return owner;
	}
	public void setGiver(String giver) {
		this.owner = giver;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String pReceiver) {
		receiver = pReceiver;
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getColorA() {
		return colorA;
	}

	public void setColorA(String color) {
		this.colorA = color;
	}

	public String isAdopted() {
		return isAdopted;
	}

	public void setAdopted(String isAdopted) {
		this.isAdopted = isAdopted;
	}

	public String getImgURL1() {
		return imgURL1;
	}

	public void setImgURL1(String imgURL1) {
		this.imgURL1 = imgURL1;
	}

	public String getImgURL2() {
		return imgURL2;
	}

	public void setImgURL2(String imgURL2) {
		this.imgURL2 = imgURL2;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}
	public void addHistory(Adoption pAdoption){
		this.history.add(pAdoption);
	}
	
	public void addDrug(String drugName, String description){
		Med newMed = new Med();
		newMed.setDescription(description);
		newMed.setName(drugName);
		this.petDrugs.add(newMed);
	}

	public ArrayList<Med> getPetDrugs() {
		return petDrugs;
	}

	public void setPetDrugs(ArrayList<Med> petDrugs) {
		this.petDrugs = petDrugs;
	}

	public ArrayList<Illness> getPetIllnes() {
		return petIllnes;
	}

	public void setPetIllnes(ArrayList<Illness> petIllnes) {
		this.petIllnes = petIllnes;
	}
	public void addIllness(String illnessName, String treatment, String description){
		Illness newIll = new Illness();
		newIll.setDescription(description);
		newIll.setName(illnessName);
		newIll.setTreatment(treatment);
		this.petIllnes.add(newIll);
	}

	public Contact getVet() {
		return vet;
	}

	public void setVet (Contact pVet) {
		this.vet=pVet;
	}
	public void addVet(String vetname, String vetProvince, String vetCity, String vetEmail, String vetPhone, String vetaddress){
		Contact vet =new Contact();
		vet.setName(vetname);
		vet.setAddress(vetaddress);
		vet.setState(vetProvince);
		vet.setEmail(vetEmail);
		vet.setCity(vetCity);
		vet.setPhone1(vetPhone);
		setVet(vet);
		
	}
	public void addContact(String vetname, String vetProvince, String vetCity, String vetEmail, String vetPhone, String vetaddress){
		Contact vet =new Contact();
		vet.setName(vetname);
		vet.setAddress(vetaddress);
		vet.setState(vetProvince);
		vet.setEmail(vetEmail);
		vet.setCity(vetCity);
		vet.setPhone1(vetPhone);
		setPetContact(vet);
	}

	public String getEnergy() {
		return energy;
	}

	public void setEnergy(String energy) {
		this.energy = energy;
	}

	public String getEaseOfTraining() {
		return easeOfTraining;
	}

	public void setEaseOfTraining(String easeOfTraining) {
		this.easeOfTraining = easeOfTraining;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getColorB() {
		return colorB;
	}

	public void setColorB(String colorB) {
		this.colorB = colorB;
	}

	public String getSpaceRequired() {
		return spaceRequired;
	}

	public void setSpaceRequired(String spaceRequired) {
		this.spaceRequired = spaceRequired;
	}

	public String getAbandonDesc() {
		return abandonDesc;
	}

	public void setAbandonDesc(String abandonDesc) {
		this.abandonDesc = abandonDesc;
	}

	public Contact getPetContact() {
		return petContact;
	}

	public void setPetContact(Contact petContact) {
		this.petContact = petContact;
	}

}
