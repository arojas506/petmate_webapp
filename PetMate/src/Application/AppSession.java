
/*
 * AppSession.java
 * Autor: Alejandro Rojas, Bernal Gonzalez
 * Descripcion: Simula una session con el servidor, usando el usario en la base (previamente autenticado) 
 * 				y la Ip del cliente.
 * 
 * 
 * 
 * 
 * 
 */
package Application;

import java.util.ArrayList;

public class AppSession {
	private String clientIP;
	private String clientUserName;
	private static ArrayList<AppSession> sessionsList= new ArrayList<AppSession>();
	private static AppSession instance;
	
	public AppSession(){
		clientIP = null;
		clientUserName = null;
	}
	
	public static AppSession getInstance(){
		if (instance==null){
			instance= new AppSession();
		}
		return instance;
	}
	
	public void startSession(String ipString, String user){
		this.clientIP= ipString;
		this.clientUserName = user;
		sessionsList.add(this);
		
	}
	public void endSession(String ipString, String user){
		for (int i=0; i < sessionsList.size(); i++){
			AppSession search = sessionsList.get(i);
			if (search.getClientIP().equals(ipString) && search.getClientUserName().equals(user)){
				sessionsList.remove(i);
			}
		}
		
	}
	public static boolean  isIpLogged(String ipString){
		if (sessionsList.contains(ipString)) return true;
		else return false;
		
	}
	void endSession(String ipString){
		if (isIpLogged(ipString)) sessionsList.remove(ipString);
	}
	
	public String getClientIP() {
		return clientIP;
	}
	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}
	public String getClientUserName() {
		return clientUserName;
	}
	public void setClientUserName(String clientUserName) {
		this.clientUserName = clientUserName;
	}
	
	public String getUsernameByIp(String clientIP){
		String clientUsername = "";
		for(int i = 0; i < sessionsList.size() ; i++  ){
			AppSession search =  sessionsList.get(i);
			if (search.getClientIP().toString().equals(clientIP)){
				clientUsername = search.getClientUserName();
			}
		}
		return clientUsername; 
		
	}
	
	public ArrayList<AppSession> getSessionList(){
		return sessionsList;		
	}

}
