package Application;

import java.util.ArrayList;

public class AdoptionQuestion {
	private int id;
	private String question;
	private ArrayList<AdoptionAnswer> answers;
	public  AdoptionQuestion( ){
		answers =  new ArrayList<AdoptionAnswer>();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public ArrayList<AdoptionAnswer> getAnswers() {
		return answers;
	}
	
	public void addAnswer(int pId, String pAnswer, int pQId){
		AdoptionAnswer newAnswer = new AdoptionAnswer();
		newAnswer.setAnswer(pAnswer);
		newAnswer.setId(pId);
		newAnswer.setAnswerId(pQId);
		answers.add(newAnswer);
		
	}
	
	

}
