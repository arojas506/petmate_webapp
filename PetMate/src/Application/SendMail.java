package Application;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class SendMail {

    public static void SendEmail(String pTo, String pFrom, String pSubject, String pContent) {

        final String username = "petmatecontact@gmail.com";
        final String password = "123456789pet";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        
        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
          });

        try {
        	String Header = "Hello!\n the user "+pFrom+ " wants to get in touch with you through the email below\n----------------------------------------------------------\n\n"; 
        			
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("petmatecontact@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(pTo));
            message.setSubject(pSubject);
            message.setText(Header+pContent);

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}